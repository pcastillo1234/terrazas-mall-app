package com.terrazas.terrazasmallap.adapters;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.squareup.picasso.Picasso;
import com.terrazas.terrazasmallap.BuildConfig;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.activities.MantenedorStandsActivity;
import com.terrazas.terrazasmallap.fragments.ProductFragment;
import com.terrazas.terrazasmallap.models.Stand;
import com.terrazas.terrazasmallap.providers.AuthProvider;
import com.terrazas.terrazasmallap.providers.StandProvider;


public class StandAdapter extends FirestoreRecyclerAdapter<Stand, StandAdapter.ViewHolder> {
    Context context;
    StandProvider standProvider;
    AuthProvider mAuthProvider;
    AlertDialog.Builder alertDialog;
    public StandAdapter(FirestoreRecyclerOptions<Stand> options, Context context){
        super(options);
        this.context = context;
    }
    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Stand stand) {
        holder.textViewtitle.setText(stand.getTitle());
        holder.textViewDescription.setText(stand.getDescription());
        holder.textViewnumber.setText(stand.getNumberStand());
        if(stand.getImage() != null){
            if(!stand.getImage().isEmpty()){
                Picasso.with(context).load(stand.getImage()).into(holder.imageViewStand);
            }
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppCompatActivity appCompatActivity = (AppCompatActivity)view.getContext();
                appCompatActivity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container,new ProductFragment(stand.getId(),stand.getTitle()))
                        .addToBackStack(null)
                        .commit();
            }
        });

        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), MantenedorStandsActivity.class);
                intent.putExtra("status",false);
                intent.putExtra("idStand",stand.getId());
                intent.putExtra("title",stand.getTitle());
                intent.putExtra("image",stand.getImage());
                intent.putExtra("numberStand",stand.getNumberStand());
                intent.putExtra("description",stand.getDescription());
                view.getContext().startActivity(intent);
            }
        });

        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog = new AlertDialog.Builder(view.getContext());
                alertDialog.setMessage("¿Realmente desea eliminar "+stand.getTitle()+"?")
                        .setCancelable(true)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                standProvider.delete(stand.getId())
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if(task.isSuccessful()){
                                                    Toast.makeText(view.getContext(),"Stand eliminado correctamente",Toast.LENGTH_SHORT).show();
                                                }else{
                                                    Toast.makeText(view.getContext(),"Error al eliminar Stand",Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                        .create()
                        .show();
            }//end onClick
        });

        holder.ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String message = "";
                    message +="Stand: "+stand.getTitle();
                    message +="\n"+"Descripción: "+stand.getDescription();
                    message +="\n"+"Descarga nuestra aplicación y conoce más sobre lo que brindamos:";
                    message +="\n"+"link";
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT, message);
                view.getContext().startActivity(Intent.createChooser(share, "Compartir Stand mediante:"));
            }//end onClick
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_stand,parent,false);
        return new StandAdapter.ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView textViewtitle;
        TextView textViewnumber;
        TextView textViewDescription;
        ImageView imageViewStand;
        CardView cardView;
        ImageView ivEdit;
        ImageView ivDelete;
        ImageView ivShare;
        public ViewHolder(View view){
            super(view);
            textViewtitle = view.findViewById(R.id.txtTitleCVStand);
            textViewnumber = view.findViewById(R.id.txtNumberCVStand);
            textViewDescription = view.findViewById(R.id.txtDescriptionCVStand);
            imageViewStand= view.findViewById(R.id.ivImageCVStand);
            cardView = view.findViewById(R.id.cardViewStand);
            ivEdit = view.findViewById(R.id.ibEditStand);
            ivDelete = view.findViewById(R.id.ibDeleteStand);
            ivShare = view.findViewById(R.id.ibShareStand);

            standProvider = new StandProvider();
            mAuthProvider = new AuthProvider();

            validarCorreo();
        }
        public void validarCorreo(){
            if(!mAuthProvider.getEmail().equals("terrazashco@hotmail.com")){
                //update
                ivEdit.setEnabled(false);
                ivEdit.setVisibility(View.INVISIBLE);
                //delete
                ivDelete.setEnabled(false);
                ivDelete.setVisibility(View.INVISIBLE);
            }
        }

    }
}
