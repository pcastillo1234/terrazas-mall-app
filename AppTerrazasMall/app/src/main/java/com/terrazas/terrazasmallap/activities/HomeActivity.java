package com.terrazas.terrazasmallap.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.fragments.CampoFragment;
import com.terrazas.terrazasmallap.fragments.CategoriaFragment;
import com.terrazas.terrazasmallap.fragments.EventsFragment;
import com.terrazas.terrazasmallap.fragments.PerfilFragment;

public class HomeActivity extends AppCompatActivity {
    BottomNavigationView bottomNavigation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        bottomNavigation = findViewById(R.id.bottom_navigation);
        bottomNavigation.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        openFragment(new CategoriaFragment());
    }


    public void openFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                   if(item.getItemId()== R.id.item_home){
                        openFragment(new CategoriaFragment());
                   }
                   else if(item.getItemId()==R.id.item_events){
                       openFragment(new EventsFragment());
                   }
                   else if (item.getItemId()==R.id.item_campo){
                       openFragment(new CampoFragment());
                   }
                   else if (item.getItemId()==R.id.item_perfil){
                       openFragment(new PerfilFragment());
                   }
                   return true;
                }
            };
}