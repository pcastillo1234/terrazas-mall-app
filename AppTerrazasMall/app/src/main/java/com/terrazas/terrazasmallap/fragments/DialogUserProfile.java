package com.terrazas.terrazasmallap.fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.activities.MantenedorCategoryActivity;
import com.terrazas.terrazasmallap.models.Category;
import com.terrazas.terrazasmallap.models.User;
import com.terrazas.terrazasmallap.providers.AuthProvider;
import com.terrazas.terrazasmallap.providers.UserProvider;

import dmax.dialog.SpotsDialog;

public class DialogUserProfile extends DialogFragment implements View.OnClickListener{
    Button btnOk;
    ImageButton ivClose;
    EditText edtUserName;
    EditText edtUserDNI;
    EditText edtUserCellphone;
    UserProvider mUserProvider;
    AuthProvider mAuthProvider;
    User user;
    String mName;
    String mDNI;
    String mCellphone;
    AlertDialog mAlertDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_user_profile,container,false);
        btnOk = view.findViewById(R.id.btnUpdateUserProfile);
        ivClose = view.findViewById(R.id.ivCloseUP);
        edtUserName = view.findViewById(R.id.edtUserName);
        edtUserDNI = view.findViewById(R.id.edtUserDNI);
        edtUserCellphone = view.findViewById(R.id.edtUserCellphone);
        mUserProvider = new UserProvider();
        mAuthProvider = new AuthProvider();

        dataForm();

        mAlertDialog = new SpotsDialog.Builder()
                .setContext(getContext())
                .setMessage("Espere un momento . . .")
                .setCancelable(false)
                .build();

        btnOk.setOnClickListener(this);
        ivClose.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        if(view == btnOk){
            mName = edtUserName.getText().toString();
            mDNI = edtUserDNI.getText().toString();
            mCellphone = edtUserCellphone.getText().toString();
                if(mDNI.length()==8 && mCellphone.length()==9 && !mName.isEmpty()){
                        updateUserProfile();
                }else if(mName.isEmpty()){
                    if(mDNI.length()!=8 && mCellphone.length() !=9){
                        edtUserName.setError("Ingrese su nombre");
                        edtUserDNI.setError("El DNI debe contar con 8 dígitos");
                        edtUserCellphone.setError("El celular debe contar con 9 dígitos");
                        edtUserName.requestFocus();
                    }else{
                        edtUserName.setError("Ingrese su nombre");
                        edtUserName.requestFocus();
                    }
                }else if(mDNI.length()!=8){
                    if(mCellphone.length() !=9){
                        edtUserDNI.setError("El DNI debe contar con 8 dígitos");
                        edtUserCellphone.setError("El celular debe contar con 9 dígitos");
                        edtUserDNI.requestFocus();
                    }else{
                        edtUserDNI.setError("El DNI debe contar con 8 dígitos");
                        edtUserDNI.requestFocus();
                    }
                }else if(mCellphone.length() !=9){
                    edtUserCellphone.setError("El celular debe contar con 9 dígitos");
                    edtUserCellphone.requestFocus();
                }
        }
        else if(view == ivClose){
            dismiss();
        }
    }//end onClick

    public void dataForm(){
        user = new User();
        mUserProvider.getUser(mAuthProvider.getUid()).addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    DocumentSnapshot doc = task.getResult();
                    if(doc.exists()){
                        user = doc.toObject(User.class);
                        edtUserName.setText(user.getUsername());
                        edtUserDNI.setText(user.getDni());
                        edtUserCellphone.setText(user.getCellphone());
                    }
                }
            }
        });
    }//end dataForm

    public void updateUserProfile(){
        mAlertDialog.show();
        User userProfile = new User();
        userProfile.setId(mAuthProvider.getUid());
        userProfile.setUsername(mName);
        userProfile.setDni(mDNI);
        userProfile.setCellphone(mCellphone);
        mUserProvider.updateUserProfile(userProfile).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> taskSave) {
                mAlertDialog.dismiss();
                if (taskSave.isSuccessful()){
                    dismiss();
                    Toast.makeText(getContext(), "Datos actualizados correctamente", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getContext(), "Error al actualizar", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }//end updateUserProfile
}
