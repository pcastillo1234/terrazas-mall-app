package com.terrazas.terrazasmallap.providers;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.terrazas.terrazasmallap.models.Events;

public class EventsProvider {
    CollectionReference mCollection;

    public EventsProvider(){
        mCollection = FirebaseFirestore.getInstance().collection("Events");
    }

    public Task<Void> save(Events events){
        return mCollection.document(events.getId()).set(events);
    }
    public Task<Void> update(Events events){
        return mCollection.document(events.getId())
                .update("idUser",events.getIdUser(),
                        "title",events.getTitle(),
                        "description",events.getDescription(),
                        "direction",events.getDirection(),
                        "price",events.getPrice(),
                        "intervalTime",events.getIntervalTime(),
                        "cellphone",events.getCellphone(),
                        "whatsapp",events.getWhatsapp(),
                        "image",events.getImage());
    }

    public Task<Void> delete(String idEvents){
        return  mCollection.document(idEvents).delete();
    }

    public Query getAll(){
        return mCollection.orderBy("title", Query.Direction.DESCENDING);
    }
}
