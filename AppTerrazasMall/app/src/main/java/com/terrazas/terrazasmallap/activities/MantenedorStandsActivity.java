package com.terrazas.terrazasmallap.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.models.Stand;
import com.terrazas.terrazasmallap.providers.AuthProvider;
import com.terrazas.terrazasmallap.providers.ImageProvider;
import com.terrazas.terrazasmallap.providers.StandProvider;
import com.terrazas.terrazasmallap.utils.FileUtil;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;

public class MantenedorStandsActivity extends AppCompatActivity {

    ImageView mImageViewStand;
    private final int GALLERY_REQUEST_CODE = 1;
    private final int PHOTO_REQUEST_CODE = 2;
    File mImageFile;
    Button mButtonStand;
    Button mButtonUpdateStand;
    ImageProvider mImageProvider;
    TextInputEditText mTextInputTitle;
    TextInputEditText mTextInputNumber;
    TextInputEditText mTextInputDescription;
    CircleImageView mCircleImageBack;
    StandProvider mStandProvider;
    String mTittle = "";
    String mNumber = "";
    String mDescription = "";
    String mIDCategory = "";
    String idStand;
    String image;
    Boolean statusBtnMain;
    AuthProvider mAutProvider;
    AlertDialog mAlertDialog;
    AlertDialog.Builder mBuilderSelector;
    CharSequence options[];

    // photo 1
    String mAbsolutePhotoPath;
    String mPhotoPath;
    File mPhotoFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_mantenedor_stands);
        mImageViewStand = findViewById(R.id.imageViewStand1);
        mImageProvider = new ImageProvider();
        mStandProvider = new StandProvider();
        mAutProvider = new AuthProvider();
        mTextInputTitle = findViewById(R.id.txtTitleStand);
        mTextInputNumber = findViewById(R.id.txtNumberStand);
        mTextInputDescription = findViewById(R.id.txtDescriptionStand);
        mButtonStand = findViewById(R.id.btnStand);
        mButtonUpdateStand = findViewById(R.id.btnUpdateStand);
        mCircleImageBack = findViewById(R.id.circleImageBackStand);

        Intent intent = getIntent();
        mIDCategory = intent.getStringExtra("idCategory");
        idStand = intent.getStringExtra("idStand");
        statusBtnMain = intent.getBooleanExtra("status",true);
        image = intent.getStringExtra("image");
        statusButton(statusBtnMain);

        mCircleImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mAlertDialog = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Espere un momento . . .")
                .setCancelable(false)
                .build();
        mBuilderSelector = new AlertDialog.Builder(this);
        mBuilderSelector.setTitle("Selecciona una opción");
         options = new  CharSequence[] {"Imagen de galeria" , "Tomar Foto"};


        mImageViewStand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selecOptionImage(1);
            }
        });

        mButtonStand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               clickStand();
            }
        });

        mButtonUpdateStand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateStand();
            }
        });
    }//end onCreate

    private void statusButton(Boolean statusButtonMaintainer) {
        if(!statusButtonMaintainer){
            this.mButtonStand.setEnabled(false);
            this.mButtonStand.setVisibility(View.INVISIBLE);
            this.mButtonUpdateStand.setEnabled(true);
            this.mButtonUpdateStand.setVisibility(View.VISIBLE);
            this.mTextInputTitle.setText(getIntent().getStringExtra("title"));
            this.mTextInputDescription.setText(getIntent().getStringExtra("description"));
            this.mTextInputNumber.setText(getIntent().getStringExtra("numberStand"));
            Picasso.with(MantenedorStandsActivity.this).load(image).into(this.mImageViewStand);
        }
    }//end statusButton

    private void updateStand() {
        mTittle = mTextInputTitle.getText().toString();
        mNumber = mTextInputNumber.getText().toString();
        mDescription = mTextInputDescription.getText().toString();
        if(!mTittle.isEmpty() && !mNumber.isEmpty() && !mDescription.isEmpty()){
            if(mImageFile!=null){
                updateImage2(mImageFile);
            }
            else if (mPhotoFile != null ) {
                updateImage2(mPhotoFile);
            }
            else{
                updateImage1();
            }
        }else if(mTittle.isEmpty()){
            if(mNumber.isEmpty() && mDescription.isEmpty()){
                mTextInputTitle.setError("Campo obligatorio");
                mTextInputNumber.setError("Campo obligatorio");
                mTextInputDescription.setError("Campo obligatorio");
                mTextInputTitle.requestFocus();
            }else{
                mTextInputTitle.setError("Campo obligatorio");
                mTextInputTitle.requestFocus();
            }
        }else if(mNumber.isEmpty()){
            if(mDescription.isEmpty()){
                mTextInputNumber.setError("Campo obligatorio");
                mTextInputDescription.setError("Campo obligatorio");
                mTextInputNumber.requestFocus();
            }else{
                mTextInputNumber.setError("Campo obligatorio");
                mTextInputNumber.requestFocus();
            }
        }else if(mDescription.isEmpty()){
            mTextInputDescription.setError("Campo obligatorio");
            mTextInputDescription.requestFocus();
        }
    }//end updateStand

    private void updateImage1() {
        mAlertDialog.show();
        Stand stand = new Stand();
        stand.setImage(image);
        stand.setId(idStand);
        stand.setTitle(mTittle);
        stand.setDescription(mDescription);
        stand.setNumberStand(mNumber);
        stand.setIdUser(mAutProvider.getUid());
        stand.setIdCategory(mIDCategory);
        mStandProvider.update(stand).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> taskSave) {
                mAlertDialog.dismiss();
                if (taskSave.isSuccessful()){
                    finish();
                    Toast.makeText(MantenedorStandsActivity.this, "Se actualizó correctamente", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(MantenedorStandsActivity.this, "No se pudo actualizar la información", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }//end updateImage1

    private void updateImage2(File imageFile1) {
        mAlertDialog.show();
        mImageProvider.save(MantenedorStandsActivity.this,imageFile1).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()){
                    mImageProvider.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            final String url = uri.toString();
                            Stand stand = new Stand();
                            stand.setImage(url);
                            stand.setId(idStand);
                            stand.setTitle(mTittle);
                            stand.setDescription(mDescription);
                            stand.setNumberStand(mNumber);
                            stand.setIdUser(mAutProvider.getUid());
                            stand.setIdCategory(mIDCategory);
                            mStandProvider.update(stand).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> taskSave) {
                                    mAlertDialog.dismiss();
                                    if (taskSave.isSuccessful()){
                                        finish();
                                        Toast.makeText(MantenedorStandsActivity.this, "Se actualizó correctamente", Toast.LENGTH_SHORT).show();
                                    }else{
                                        Toast.makeText(MantenedorStandsActivity.this, "No se pudo actualizar la información", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                    });
                }else{
                    mAlertDialog.dismiss();
                    Toast.makeText(MantenedorStandsActivity.this, "Error al actualizar la imagen", Toast.LENGTH_LONG).show();
                }
            }
        });

    }// end updateImage2

    private void clickStand() {
        mTittle = mTextInputTitle.getText().toString();
        mNumber = mTextInputNumber.getText().toString();
        mDescription = mTextInputDescription.getText().toString();
        if(!mTittle.isEmpty() && !mNumber.isEmpty() && !mDescription.isEmpty()){
            if(mImageFile!=null){
                saveImage(mImageFile );
            }
            else if (mPhotoFile != null ) {
                saveImage(mPhotoFile);
            }
            else{
                Toast.makeText(this, "Debes seleccionar una imagen", Toast.LENGTH_SHORT).show();
            }
        }else if(mTittle.isEmpty()){
            if(mNumber.isEmpty() && mDescription.isEmpty()){
                mTextInputTitle.setError("Campo obligatorio");
                mTextInputNumber.setError("Campo obligatorio");
                mTextInputDescription.setError("Campo obligatorio");
                mTextInputTitle.requestFocus();
            }else{
                mTextInputTitle.setError("Campo obligatorio");
                mTextInputTitle.requestFocus();
            }
        }else if(mNumber.isEmpty()){
            if(mDescription.isEmpty()){
                mTextInputNumber.setError("Campo obligatorio");
                mTextInputDescription.setError("Campo obligatorio");
                mTextInputNumber.requestFocus();
            }else{
                mTextInputNumber.setError("Campo obligatorio");
                mTextInputNumber.requestFocus();
            }
        }else if(mDescription.isEmpty()){
            mTextInputDescription.setError("Campo obligatorio");
            mTextInputDescription.requestFocus();
        }

    }//end clickStand

    private void saveImage(File imageFile1) {
        mAlertDialog.show();
        mImageProvider.save(MantenedorStandsActivity.this,imageFile1).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()){
                    mImageProvider.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            final String url = uri.toString();
                            Stand stand = new Stand();
                            stand.setImage(url);
                            stand.setId(UUID.randomUUID().toString());
                            stand.setTitle(mTittle);
                            stand.setDescription(mDescription);
                            stand.setNumberStand(mNumber);
                            stand.setIdUser(mAutProvider.getUid());
                            stand.setIdCategory(mIDCategory);
                            mStandProvider.save(stand).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> taskSave) {
                                    mAlertDialog.dismiss();
                                    if (taskSave.isSuccessful()){
                                        //clearForm();
                                        finish();
                                        Toast.makeText(MantenedorStandsActivity.this, "Se almacenó correctamente", Toast.LENGTH_SHORT).show();
                                    }else{
                                        Toast.makeText(MantenedorStandsActivity.this, "No se pudo almacenar la información", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                    });
                }else{
                    mAlertDialog.dismiss();
                    Toast.makeText(MantenedorStandsActivity.this, "Error al almacenar la imagen", Toast.LENGTH_LONG).show();
                }
            }
        });

    }// end saveImage

    private void clearForm() {
        mTextInputTitle.setText("");
        mTextInputNumber.setText("");
        mTextInputDescription.setText("");
        mImageViewStand.setImageResource(R.drawable.ic_camera_24);
        mTittle =  "";
        mNumber = "";
        mDescription = "";
        mImageFile = null;
    }//end clearForm

    private void selecOptionImage(final int numberImage) {
        mBuilderSelector.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == 0) {
                    if (numberImage == 1) {
                        openGalery(GALLERY_REQUEST_CODE);
                    }
                }
                else if (i == 1){
                    if (numberImage == 1) {
                        takePhoto(PHOTO_REQUEST_CODE);
                    }
                }
            }
        });
        mBuilderSelector.show();
    }//end selectOptionImage

    private void openGalery(int requestCode) {
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent,requestCode);
    }//end openGallery

    private void takePhoto( int requestCode) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createPhotoFile(requestCode);
            } catch(Exception e) {
                Toast.makeText(this, "Hubo un error con el archivo " + e.getMessage(), Toast.LENGTH_LONG).show();
            }

            if (photoFile != null) {
                Uri photoUri = FileProvider.getUriForFile(MantenedorStandsActivity.this, "com.terrazas.terrazasmallap", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(takePictureIntent, requestCode);
            }
        } Toast.makeText(this, "Seleccionó tomo foto", Toast.LENGTH_SHORT).show();
    }//end takePhoto

    private File createPhotoFile(int requestCode) throws IOException {
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File photoFile = File.createTempFile(
                new Date() + "_photo",
                ".jpg",
                storageDir
        );
        if (requestCode == PHOTO_REQUEST_CODE) {
            mPhotoPath = "file:" + photoFile.getAbsolutePath();
            mAbsolutePhotoPath = photoFile.getAbsolutePath();
        }

        return photoFile;
    }// end createPhotoFile

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK){
            try {
                mPhotoFile = null;
                mImageFile = FileUtil.from(this,data.getData());
                mImageViewStand.setImageBitmap(BitmapFactory.decodeFile(mImageFile.getAbsolutePath()));
            }catch (Exception e){
                Log.d("Error", "se produjo un error" + e.getMessage());
                Toast.makeText(this, "Se produjo un error" + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }

        /**
         * SELECCION DE FOTOGRAFIA
         */
        if (requestCode == PHOTO_REQUEST_CODE && resultCode == RESULT_OK) {
            mImageFile = null;
            mPhotoFile = new File(mAbsolutePhotoPath);
            Picasso.with(MantenedorStandsActivity.this).load(mPhotoPath).into(mImageViewStand);
        }
    }//end onActivityResult
}