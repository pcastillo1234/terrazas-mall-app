package com.terrazas.terrazasmallap.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.adapters.ReservationListAdapter;
import com.terrazas.terrazasmallap.adapters.ScheduleAdapter;
import com.terrazas.terrazasmallap.models.FieldReservation;
import com.terrazas.terrazasmallap.models.Schedule;
import com.terrazas.terrazasmallap.models.User;
import com.terrazas.terrazasmallap.providers.AuthProvider;
import com.terrazas.terrazasmallap.providers.FieldProvider;
import com.terrazas.terrazasmallap.providers.FieldReservationProvider;
import com.terrazas.terrazasmallap.providers.UserProvider;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class SportFieldReservationFragment extends Fragment {
    Button btnContinue;
    EditText edtName;
    EditText edtDNI;
    EditText edtCellphone;
    TextView edtDate;
    String mName;
    String mDNI;
    String mCellphone;
    String mDate;
    AuthProvider mAuthProvider;
    UserProvider mUserProvider;
    User user;
    RecyclerView mRecyclerView;
    ScheduleAdapter mScheduleAdapter;
    List<Schedule> list;
    private static final String CERO = "0";
    private static final String BARRA = "/";
    public final Calendar calendar = Calendar.getInstance();
    public final Calendar currentMonth = Calendar.getInstance();
    final int month = calendar.get(Calendar.MONTH);
    final int day = calendar.get(Calendar.DAY_OF_MONTH);
    final int lastDayOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    final int year = calendar.get(Calendar.YEAR);
    String idField;
    String title;
    String price;
    String timeInterval;
    String startTime;
    String endTime;
    String mCode;
    FieldReservationProvider mFieldReservationProvider;
    List<FieldReservation> mFieldReservationList;
    List<Schedule> mScheduleSelectedList;

    public SportFieldReservationFragment() {
    }

    public SportFieldReservationFragment(String idField,String title, String price,String timeInterval, String startTime, String endTime) {
        this.idField = idField;
        this.title = title;
        this.price = price;
        this.timeInterval = timeInterval;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_sport_field_reservation,container,false);
        edtName = root.findViewById(R.id.edtNameSFR);
        edtDNI = root.findViewById(R.id.edtDNISFR);
        edtCellphone = root.findViewById(R.id.edtCellphoneSFR);
        edtDate = root.findViewById(R.id.edtDateSFR);
        mRecyclerView = root.findViewById(R.id.recyclerViewSchedule);
        btnContinue = root.findViewById(R.id.btnConfirmSFR);
        mAuthProvider = new AuthProvider();
        mUserProvider = new UserProvider();
        mFieldReservationProvider = new FieldReservationProvider();

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(),3);
        mRecyclerView.setLayoutManager(gridLayoutManager);

        edtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDateSelected();
            }
        });
        
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmReservation();
            }
        });

        return root;
    }

    private void confirmReservation() {
        mName = edtName.getText().toString();
        mDNI = edtDNI.getText().toString();
        mCellphone = edtCellphone.getText().toString();
        mDate = edtDate.getText().toString();
        if(mDNI.length()==8 && mCellphone.length()==9 && !mName.isEmpty() && !mDate.isEmpty()){
            mCode = mScheduleAdapter.getCode();
            if(!mCode.isEmpty()){
                AppCompatActivity activity = (AppCompatActivity)getContext();
                activity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container,new ConfirmFieldReservation(mName,mDNI,mCellphone,idField,title,mDate,price,timeInterval,startTime,endTime,mCode,list))
                        .addToBackStack(null)
                        .commit();
            }else{
                Toast.makeText(getContext(),"Seleccionar mínimo un horario",Toast.LENGTH_SHORT).show();
            }
        }else if(mName.isEmpty()){
            if(mDNI.length()!=8 && mCellphone.length() !=9){
                edtName.setError("Ingrese su nombre");
                edtDNI.setError("El DNI debe contar con 8 dígitos");
                edtCellphone.setError("El celular debe contar con 9 dígitos");
                edtName.requestFocus();
            }else{
                edtName.setError("Ingrese su nombre");
                edtName.requestFocus();
            }
        }else if(mDNI.length()!=8){
            if(mCellphone.length() !=9){
                edtDNI.setError("El DNI debe contar con 8 dígitos");
                edtCellphone.setError("El celular debe contar con 9 dígitos");
                edtDNI.requestFocus();
            }else{
                edtDNI.setError("El DNI debe contar con 8 dígitos");
                edtDNI.requestFocus();
            }
        }else if(mCellphone.length() !=9){
                edtCellphone.setError("El celular debe contar con 9 dígitos");
                edtCellphone.requestFocus();
        }else if(mDate.isEmpty()){
            Toast.makeText(getContext(),"Seleccione una fecha",Toast.LENGTH_SHORT).show();
        }
    }//end confirmReservation

    private void getDateSelected() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                final int monthOfYear = month + 1;
                String formatDay = (dayOfMonth < 10)? CERO + String.valueOf(dayOfMonth):String.valueOf(dayOfMonth);
                String formatMonth = (monthOfYear < 10)? CERO + String.valueOf(monthOfYear):String.valueOf(monthOfYear);
                edtDate.setText(formatDay + BARRA + formatMonth + BARRA + year);
                //getReservationForDay()
                getReservationForDay();
            }
        },year, month, day);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        currentMonth.set( year, month, lastDayOfMonth, 0, 0, 0 );
        datePickerDialog.getDatePicker().setMaxDate(currentMonth.getTimeInMillis());
        datePickerDialog.show();
    }

    private void getReservationForDay() {
        mFieldReservationProvider.getReservationForIDField(idField,edtDate.getText().toString()).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                mFieldReservationList = new ArrayList<>();
                mScheduleSelectedList = new ArrayList<>();
                if(task.isSuccessful()){
                    for(QueryDocumentSnapshot document : task.getResult()){
                        FieldReservation mData = document.toObject(FieldReservation.class);
                        mFieldReservationList.add(mData);
                    }//end for
                    for(FieldReservation fieldReservation : mFieldReservationList){
                        String[] idSchedule = fieldReservation.getIdSchedule().split(",");
                        for(int i = 0; i<idSchedule.length;i++){
                            mScheduleSelectedList.add(new Schedule(idSchedule[i],"time"));
                        }
                    }
                    for(Schedule schedule : mScheduleSelectedList){
                        Log.d("Selected Schedule List",schedule.getIdSchedule()+" - "+schedule.getTime());
                    }
                    //Adapter
                    mScheduleAdapter = new ScheduleAdapter(list,mScheduleSelectedList);
                    mRecyclerView.setAdapter(mScheduleAdapter);
                }else{
                    Log.d("Error","Error getting documents: ",task.getException());
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        //Schedule
        scheduleList();
        //user data
        user = new User();
        mUserProvider.getUser(mAuthProvider.getUid()).addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    DocumentSnapshot doc = task.getResult();
                    if(doc.exists()){
                        user = doc.toObject(User.class);
                        edtName.setText(user.getUsername());
                        edtDNI.setText(user.getDni());
                        edtCellphone.setText(user.getCellphone());
                    }
                }
            }
        });
    }//end onStart

    public void scheduleList(){
        int con = 1;
        list = new ArrayList<>();
        list.add(new Schedule(String.valueOf(con),startTime));
        //startTime
        String[] a = startTime.split(":");
        int a0 = Integer.parseInt(a[0]) * 3600;
        int a1 = Integer.parseInt(a[1]) * 60;
        int a2 = a0+a1;
        //endTime
        String[] b = endTime.split(":");
        int b0 = Integer.parseInt(b[0]) * 3600;
        int b1 = Integer.parseInt(b[1]) * 60;
        int b2 = b0+b1;
        //timeInterval
        int c = Integer.parseInt(timeInterval) * 60;
        //newHour
        int d = 0;
        //newMinute
        int e = 0;
        //newTime
        String newTime = "";
        while(a2 < (b2-c)){
            a2 += c;
            con++;
            d = a2/3600;
            e = (a2%3600)/60;
            if(d<10){
                newTime = "0"+String.valueOf(d)+":";
            }else{
                newTime = String.valueOf(d)+":";
            }
            if(e<10){
                newTime += "0"+String.valueOf(e);
            }else{
                newTime += String.valueOf(e);
            }
            list.add(new Schedule(String.valueOf(con),newTime));
        }
        for(Schedule schedule : list){
            Log.d("Schedule List",schedule.getIdSchedule()+" - "+schedule.getTime());
        }
    }//scheduleList
}
