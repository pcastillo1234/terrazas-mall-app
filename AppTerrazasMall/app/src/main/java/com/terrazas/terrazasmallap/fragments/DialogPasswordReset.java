package com.terrazas.terrazasmallap.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.providers.AuthProvider;

public class DialogPasswordReset extends DialogFragment implements View.OnClickListener {
    
    TextInputEditText mTextInputEditTextEmail;
    ImageButton ivClose;
    Button btnSendEmail;
    String mEmail;
    AuthProvider mAuthProvider;
    
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_password_reset,container,false);
        mTextInputEditTextEmail = view.findViewById(R.id.txtSendEmail);
        btnSendEmail = view.findViewById(R.id.btnPasswordReset);
        ivClose = view.findViewById(R.id.ivClosePR);
        mAuthProvider = new AuthProvider();
        btnSendEmail.setOnClickListener(this);
        ivClose.setOnClickListener(this);
        
        return view;
    }

    @Override
    public void onClick(View view) {
        if(view == btnSendEmail){
            mEmail = mTextInputEditTextEmail.getText().toString();
            if(!mEmail.isEmpty()){
                sendEmailPasswordReset();
            }else{
                mTextInputEditTextEmail.setError("Campo obligatorio");
            }
        }else if(view == ivClose){
            dismiss();
        }
    }

    private void sendEmailPasswordReset() {
        mAuthProvider.sendEmailToPasswordReset(mTextInputEditTextEmail.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    dismiss();
                    Toast.makeText(getContext(),"Correo enviado",Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getContext(),"Error al enviar",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
