package com.terrazas.terrazasmallap.activities;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.models.Events;
import com.terrazas.terrazasmallap.models.Field;
import com.terrazas.terrazasmallap.providers.AuthProvider;
import com.terrazas.terrazasmallap.providers.FieldProvider;
import com.terrazas.terrazasmallap.providers.ImageProvider;
import com.terrazas.terrazasmallap.utils.FileUtil;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;

public class MantenedorCampoActivity extends AppCompatActivity {

    ImageView mImageViewField;
    private final int GALLERY_REQUEST_CODE = 1;
    private final int PHOTO_REQUEST_CODE = 2;
    File mImageFile;
    Button mButtonField;
    Button mButtonUpdateField;
    ImageProvider mImageProvider;
    TextInputEditText mTextInputTitle;
    TextInputEditText mTextInputDescription;
    TextInputEditText mTextInputDirection;
    TextInputEditText mTextInputPrice;
    TextInputEditText mTextInputTimeInterval;
    TextView mTextViewStartTime;
    TextView mTextViewEndTime;
    CircleImageView mCircleImageBack;
    FieldProvider mFieldProvider;
    String mTitle;
    String mDescription;
    String mDirection;
    String mPrice;
    String idField;
    String image;
    String mDays;
    String mTimeInterval;
    String mStartTime;
    String mEndTime;
    CheckBox mcbLunes;
    CheckBox mcbMartes;
    CheckBox mcbMiercoles;
    CheckBox mcbJueves;
    CheckBox mcbViernes;
    CheckBox mcbSabado;
    CheckBox mcbDomingo;
    Boolean statusBtnMain;
    AuthProvider mAutProvider;
    AlertDialog mAlertDialog;
    AlertDialog.Builder mBuilderSelector;
    CharSequence options[];

    // photo 1
    String mAbsolutePhotoPath;
    String mPhotoPath;
    File mPhotoFile;

    //hour
    private static final String ZERO = "0";
    private static final String TWO_POINT = ":";
    public final Calendar calendar = Calendar.getInstance();
    final int hour = calendar.get(Calendar.HOUR_OF_DAY);
    final int minute = calendar.get(Calendar.MINUTE);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_mantenedor_campo);
        mImageViewField = findViewById(R.id.imageViewFields1);
        mImageProvider = new ImageProvider();
        mFieldProvider = new FieldProvider();
        mAutProvider = new AuthProvider();
        mTextInputTitle = findViewById(R.id.txtTitleCD);
        mTextInputDescription = findViewById(R.id.txtDescriptionCD);
        mTextInputDirection =findViewById(R.id.txtDirectionCD);
        mTextInputPrice =findViewById(R.id.txtPriceCD);
        mTextInputTimeInterval = findViewById(R.id.txtTimeIntervalCD);
        mTextViewStartTime =findViewById(R.id.txtStartTimeCD);
        mTextViewEndTime =findViewById(R.id.txtEndTimeCD);
        mButtonField = findViewById(R.id.btnFields);
        mButtonUpdateField = findViewById(R.id.btnUpdateFields);
        mCircleImageBack = findViewById(R.id.circleImageBackCD);
        mcbLunes = findViewById(R.id.cbLunes);
        mcbMartes = findViewById(R.id.cbMartes);
        mcbMiercoles = findViewById(R.id.cbMiercoles);
        mcbJueves = findViewById(R.id.cbJueves);
        mcbViernes = findViewById(R.id.cbViernes);
        mcbSabado = findViewById(R.id.cbSabado);
        mcbDomingo = findViewById(R.id.cbDomingo);

        Intent intent = getIntent();
        idField = intent.getStringExtra("idField");
        statusBtnMain = intent.getBooleanExtra("status",true);
        image = intent.getStringExtra("image");
        statusButton(statusBtnMain);

        mCircleImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        mAlertDialog = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Espere un momento . . .")
                .setCancelable(false)
                .build();
        mBuilderSelector = new AlertDialog.Builder(this);
        mBuilderSelector.setTitle("Selecciona una opción");
        options = new  CharSequence[] {"Imagen de galeria" , "Tomar Foto"};

        mImageViewField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectOptionImage(1);
            }
        });
        mButtonField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickField();
            }
        });

        mButtonUpdateField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateField();
            }
        });

        mTextViewStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getStartHour();
            }
        });

        mTextViewEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getEndHour();
            }
        });
    }//end onCreate

    private void getEndHour() {
        TimePickerDialog tpdEndHour = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String formatHour =  (hourOfDay < 10)? String.valueOf(ZERO + hourOfDay) : String.valueOf(hourOfDay);
                String formatMinute = (minute < 10)? String.valueOf(ZERO + minute):String.valueOf(minute);
                mTextViewEndTime.setText(formatHour + TWO_POINT + formatMinute);
            }
        }, hour, minute, true);
        tpdEndHour.show();
    }

    private void getStartHour() {
        TimePickerDialog tpdStartHour = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String formatHour =  (hourOfDay < 10)? String.valueOf(ZERO + hourOfDay) : String.valueOf(hourOfDay);
                String formatMinute = (minute < 10)? String.valueOf(ZERO + minute):String.valueOf(minute);
                mTextViewStartTime.setText(formatHour + TWO_POINT + formatMinute);
            }
        }, hour, minute, true);
        tpdStartHour.show();
    }

    private void updateField() {
        mTitle = mTextInputTitle.getText().toString();
        mDescription = mTextInputDescription.getText().toString();
        mDirection = mTextInputDirection.getText().toString();
        mPrice = mTextInputPrice.getText().toString();
        mTimeInterval = mTextInputTimeInterval.getText().toString();
        mStartTime = mTextViewStartTime.getText().toString();
        mEndTime = mTextViewEndTime.getText().toString();
        mDays="";
        daySelected();

        //first
        if(!mTitle.isEmpty() && !mDescription.isEmpty() && !mDirection.isEmpty() && !mPrice.isEmpty()&& !mTimeInterval.isEmpty()){
            //second
            if(!mStartTime.isEmpty() && !mEndTime.isEmpty()){
                //third
                if(!validateTime(mStartTime,mEndTime,mTimeInterval)){
                    //fourth
                    if(mcbLunes.isChecked() || mcbMartes.isChecked() || mcbMiercoles.isChecked() || mcbJueves.isChecked() || mcbViernes.isChecked() || mcbSabado.isChecked() || mcbDomingo.isChecked()){
                        if(mImageFile!=null){
                            updateImage2(mImageFile);
                        }
                        else if (mPhotoFile != null ) {
                            updateImage2(mPhotoFile);
                        }
                        else{
                            updateImage1();
                        }
                    }else{
                        Toast.makeText(this, "Seleccione mínimo 1 día", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(),"Seleccione un horario válido",Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(getApplicationContext(),"Seleccione un horario",Toast.LENGTH_SHORT).show();
            }
        }else if(mTitle.isEmpty()){
            if(mDescription.isEmpty() && mDirection.isEmpty() && mPrice.isEmpty()&& mTimeInterval.isEmpty()){
                mTextInputTitle.setError("Campo obligatorio");
                mTextInputDescription.setError("Campo obligatorio");
                mTextInputDirection.setError("Campo obligatorio");
                mTextInputPrice.setError("Campo obligatorio");
                mTextInputTimeInterval.setError("Campo obligatorio");
                mTextInputTitle.requestFocus();
            }else{
                mTextInputTitle.setError("Campo obligatorio");
                mTextInputTitle.requestFocus();
            }
        }else if(mDescription.isEmpty()){
            if(mDirection.isEmpty() && mPrice.isEmpty()&& mTimeInterval.isEmpty()){
                mTextInputDescription.setError("Campo obligatorio");
                mTextInputDirection.setError("Campo obligatorio");
                mTextInputPrice.setError("Campo obligatorio");
                mTextInputTimeInterval.setError("Campo obligatorio");
                mTextInputDescription.requestFocus();
            }else{
                mTextInputDescription.setError("Campo obligatorio");
                mTextInputDescription.requestFocus();
            }
        }else if(mDirection.isEmpty()){
            if(mPrice.isEmpty() && mTimeInterval.isEmpty()){
                mTextInputDirection.setError("Campo obligatorio");
                mTextInputPrice.setError("Campo obligatorio");
                mTextInputTimeInterval.setError("Campo obligatorio");
                mTextInputDirection.requestFocus();
            }else{
                mTextInputDirection.setError("Campo obligatorio");
                mTextInputDirection.requestFocus();
            }
        }else if(mPrice.isEmpty()){
            if(mTimeInterval.isEmpty()){
                mTextInputPrice.setError("Campo obligatorio");
                mTextInputTimeInterval.setError("Campo obligatorio");
                mTextInputPrice.requestFocus();
            }else{
                mTextInputPrice.setError("Campo obligatorio");
                mTextInputPrice.requestFocus();
            }
        }else if(mTimeInterval.isEmpty()){
            mTextInputTimeInterval.setError("Campo obligatorio");
            mTextInputTimeInterval.requestFocus();
        }

    }//end updateField

    private void daySelected() {
        if(mcbLunes.isChecked()){
            mDays += "Lunes";
        }if(mcbMartes.isChecked()){
            if(mDays.isEmpty()){
                mDays += "Martes";
            }else{
                mDays += ", Martes";
            }
        }if(mcbMiercoles.isChecked()){
            if(mDays.isEmpty()){
                mDays += "Miércoles";
            }else{
                mDays += ", Miércoles";
            }
        }if(mcbJueves.isChecked()){
            if(mDays.isEmpty()){
                mDays += "Jueves";
            }else{
                mDays += ", Jueves";
            }
        }if(mcbViernes.isChecked()){
            if(mDays.isEmpty()){
                mDays += "Viernes";
            }else{
                mDays += ", Viernes";
            }
        }if(mcbSabado.isChecked()){
            if(mDays.isEmpty()){
                mDays += "Sábado";
            }else{
                mDays += ", Sábado";
            }
        }if(mcbDomingo.isChecked()){
            if(mDays.isEmpty()){
                mDays += "Domingo";
            }else{
                mDays += ", Domingo";
            }
        }
    }

    private void updateImage1() {
        mAlertDialog.show();
        Field field = new Field();
        field.setImage(image);
        field.setId(idField);
        field.setTitle(mTitle);
        field.setDescription(mDescription);
        field.setDirection(mDirection);
        field.setPrice(mPrice);
        field.setTimeInterval(mTimeInterval);
        field.setStartTime(mStartTime);
        field.setEndTime(mEndTime);
        field.setDays(mDays);
        field.setIdUser(mAutProvider.getUid());
        mFieldProvider.update(field).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> taskSave) {
                mAlertDialog.dismiss();
                if (taskSave.isSuccessful()){
                    finish();
                    Toast.makeText(MantenedorCampoActivity.this, "Se actualizó correctamente", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(MantenedorCampoActivity.this, "No se pudo actualizar la información", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }//end updateImage1

    private void updateImage2(File imageFile1) {
        mAlertDialog.show();
        mImageProvider.save(MantenedorCampoActivity.this,imageFile1).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()){
                    mImageProvider.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            final String url = uri.toString();
                            Field field = new Field();
                            field.setImage(url);
                            field.setId(idField);
                            field.setTitle(mTitle);
                            field.setDescription(mDescription);
                            field.setDirection(mDirection);
                            field.setPrice(mPrice);
                            field.setTimeInterval(mTimeInterval);
                            field.setStartTime(mStartTime);
                            field.setEndTime(mEndTime);
                            field.setDays(mDays);
                            field.setIdUser(mAutProvider.getUid());
                            mFieldProvider.update(field).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> taskSave) {
                                    mAlertDialog.dismiss();
                                    if (taskSave.isSuccessful()){
                                        finish();
                                        Toast.makeText(MantenedorCampoActivity.this, "Se actualizó correctamente", Toast.LENGTH_SHORT).show();
                                    }else{
                                        Toast.makeText(MantenedorCampoActivity.this, "No se pudo actualizar la información", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                    });
                }else{
                    mAlertDialog.dismiss();
                    Toast.makeText(MantenedorCampoActivity.this, "Error al actualizar la imagen", Toast.LENGTH_LONG).show();
                }
            }
        });
    }//end updateImage2

    private void statusButton(Boolean statusButtonMaintainer) {
        if(!statusButtonMaintainer){
            this.mButtonField.setEnabled(false);
            this.mButtonField.setVisibility(View.INVISIBLE);
            this.mButtonUpdateField.setEnabled(true);
            this.mButtonUpdateField.setVisibility(View.VISIBLE);
            this.mTextInputTitle.setText(getIntent().getStringExtra("title"));
            this.mTextInputDescription.setText(getIntent().getStringExtra("description"));
            this.mTextInputDirection.setText(getIntent().getStringExtra("direction"));
            this.mTextInputPrice.setText(getIntent().getStringExtra("price"));
            this.mTextInputTimeInterval.setText(getIntent().getStringExtra("timeInterval"));
            this.mTextViewStartTime.setText(getIntent().getStringExtra("startTime"));
            this.mTextViewEndTime.setText(getIntent().getStringExtra("endTime"));
            Picasso.with(MantenedorCampoActivity.this).load(image).into(this.mImageViewField);
            setCheckBox();
        }
    }//end statusButton

    private void setCheckBox() {
        String day = getIntent().getStringExtra("days");
        if(day.toLowerCase().contains("lunes")){
            this.mcbLunes.setChecked(true);
        }
        if(day.toLowerCase().contains("martes")){
            this.mcbMartes.setChecked(true);
        }
        if(day.toLowerCase().contains("miércoles")){
            this.mcbMiercoles.setChecked(true);
        }
        if(day.toLowerCase().contains("jueves")){
            this.mcbJueves.setChecked(true);
        }
        if(day.toLowerCase().contains("viernes")){
            this.mcbViernes.setChecked(true);
        }
        if(day.toLowerCase().contains("sábado")){
            this.mcbSabado.setChecked(true);
        }
        if(day.toLowerCase().contains("domingo")){
            this.mcbDomingo.setChecked(true);
        }
    }

    private void clickField() {
        mTitle = mTextInputTitle.getText().toString();
        mDescription = mTextInputDescription.getText().toString();
        mDirection = mTextInputDirection.getText().toString();
        mPrice = mTextInputPrice.getText().toString();
        mTimeInterval = mTextInputTimeInterval.getText().toString();
        mStartTime = mTextViewStartTime.getText().toString();
        mEndTime = mTextViewEndTime.getText().toString();
        mDays="";
        daySelected();

        //first
        if(!mTitle.isEmpty() && !mDescription.isEmpty() && !mDirection.isEmpty() && !mPrice.isEmpty()&& !mTimeInterval.isEmpty()){
            //second
            if(!mStartTime.isEmpty() && !mEndTime.isEmpty()){
                //third
                if(!validateTime(mStartTime,mEndTime,mTimeInterval)){
                    //fourth
                    if(mcbLunes.isChecked() || mcbMartes.isChecked() || mcbMiercoles.isChecked() || mcbJueves.isChecked() || mcbViernes.isChecked() || mcbSabado.isChecked() || mcbDomingo.isChecked()){
                        if(mImageFile!=null){
                            saveImage(mImageFile );
                        }
                        else if (mPhotoFile != null ) {
                            saveImage(mPhotoFile);
                        }
                        else{
                            Toast.makeText(this, "Debes seleccionar una imagen", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(this, "Seleccione mínimo 1 día", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(),"Seleccione un horario válido",Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(getApplicationContext(),"Seleccione un horario",Toast.LENGTH_SHORT).show();
            }
        }else if(mTitle.isEmpty()){
            if(mDescription.isEmpty() && mDirection.isEmpty() && mPrice.isEmpty()&& mTimeInterval.isEmpty()){
                mTextInputTitle.setError("Campo obligatorio");
                mTextInputDescription.setError("Campo obligatorio");
                mTextInputDirection.setError("Campo obligatorio");
                mTextInputPrice.setError("Campo obligatorio");
                mTextInputTimeInterval.setError("Campo obligatorio");
                mTextInputTitle.requestFocus();
            }else{
                mTextInputTitle.setError("Campo obligatorio");
                mTextInputTitle.requestFocus();
            }
        }else if(mDescription.isEmpty()){
            if(mDirection.isEmpty() && mPrice.isEmpty()&& mTimeInterval.isEmpty()){
                mTextInputDescription.setError("Campo obligatorio");
                mTextInputDirection.setError("Campo obligatorio");
                mTextInputPrice.setError("Campo obligatorio");
                mTextInputTimeInterval.setError("Campo obligatorio");
                mTextInputDescription.requestFocus();
            }else{
                mTextInputDescription.setError("Campo obligatorio");
                mTextInputDescription.requestFocus();
            }
        }else if(mDirection.isEmpty()){
            if(mPrice.isEmpty() && mTimeInterval.isEmpty()){
                mTextInputDirection.setError("Campo obligatorio");
                mTextInputPrice.setError("Campo obligatorio");
                mTextInputTimeInterval.setError("Campo obligatorio");
                mTextInputDirection.requestFocus();
            }else{
                mTextInputDirection.setError("Campo obligatorio");
                mTextInputDirection.requestFocus();
            }
        }else if(mPrice.isEmpty()){
            if(mTimeInterval.isEmpty()){
                mTextInputPrice.setError("Campo obligatorio");
                mTextInputTimeInterval.setError("Campo obligatorio");
                mTextInputPrice.requestFocus();
            }else{
                mTextInputPrice.setError("Campo obligatorio");
                mTextInputPrice.requestFocus();
            }
        }else if(mTimeInterval.isEmpty()){
            mTextInputTimeInterval.setError("Campo obligatorio");
            mTextInputTimeInterval.requestFocus();
        }
    }//end clickField

    private boolean validateTime(String mStartTime, String mEndTime, String mTimeInterval) {
        //startTime
        String[] a = mStartTime.split(":");
        int a0 = Integer.parseInt(a[0]) * 3600;
        int a1 = Integer.parseInt(a[1]) * 60;
        int a2 = a0+a1;
        //endTime
        String[] b = mEndTime.split(":");
        int b0 = Integer.parseInt(b[0]) * 3600;
        int b1 = Integer.parseInt(b[1]) * 60;
        int b2 = b0+b1;
        //timeInterval
        int c = Integer.parseInt(mTimeInterval) * 60;
        if((a2+c) <= b2){
            return false;
        }else{
            return true;
        }
    }

    private void saveImage(File imageFile1) {
        mAlertDialog.show();
        mImageProvider.save(MantenedorCampoActivity.this,imageFile1).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()){
                    mImageProvider.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            final String url = uri.toString();
                            Field field = new Field();
                            field.setImage(url);
                            field.setId(UUID.randomUUID().toString());
                            field.setTitle(mTitle);
                            field.setDescription(mDescription);
                            field.setDirection(mDirection);
                            field.setPrice(mPrice);
                            field.setTimeInterval(mTimeInterval);
                            field.setStartTime(mStartTime);
                            field.setEndTime(mEndTime);
                            field.setDays(mDays);
                            field.setIdUser(mAutProvider.getUid());
                            mFieldProvider.save(field).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> taskSave) {
                                    mAlertDialog.dismiss();
                                    if (taskSave.isSuccessful()){
                                        //clearForm();
                                        finish();
                                        Toast.makeText(MantenedorCampoActivity.this, "Se almacenó correctamente", Toast.LENGTH_SHORT).show();
                                    }else{
                                        Toast.makeText(MantenedorCampoActivity.this, "No se pudo alamacenar la información", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                    });
                }else{
                    mAlertDialog.dismiss();
                    Toast.makeText(MantenedorCampoActivity.this, "Error al almacenar la imagen", Toast.LENGTH_LONG).show();
                }
            }
        });
    }//end saveImage

    private void clearForm() {
        mTextInputTitle.setText("");
        mTextInputDescription.setText("");
        mTextInputDirection.setText("");
        mTextInputPrice.setText("");
        mTextInputTimeInterval.setText("");
        mTextViewStartTime.setText("");
        mTextViewEndTime.setText("");
        mImageViewField.setImageResource(R.drawable.ic_camera_24);
        mTitle =  "";
        mDescription = "";
        mDirection = "";
        mPrice = "";
        mTimeInterval = "";
        mStartTime = "";
        mEndTime = "";
        mImageFile = null;
        mDays = "";
        mcbLunes.setChecked(false);
        mcbMartes.setChecked(false);
        mcbMiercoles.setChecked(false);
        mcbJueves.setChecked(false);
        mcbViernes.setChecked(false);
        mcbSabado.setChecked(false);
        mcbDomingo.setChecked(false);

    }//end clearForm

    private void selectOptionImage(final int numberImage) {
        mBuilderSelector.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == 0) {
                    if (numberImage == 1) {
                        openGallery(GALLERY_REQUEST_CODE);
                    }
                }
                else if (i == 1){
                    if (numberImage == 1) {
                        takePhoto(PHOTO_REQUEST_CODE);
                    }
                }
            }
        });
        mBuilderSelector.show();
    }//end selectOptionImage

    private void openGallery(int gallery_request_code) {
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent,gallery_request_code);
    }//end openGallery

    private void takePhoto(int photo_request_code) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createPhotoFile(photo_request_code);
            } catch(Exception e) {
                Toast.makeText(this, "Hubo un error con el archivo " + e.getMessage(), Toast.LENGTH_LONG).show();
            }

            if (photoFile != null) {
                Uri photoUri = FileProvider.getUriForFile(MantenedorCampoActivity.this, "com.terrazas.terrazasmallap", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(takePictureIntent, photo_request_code);
            }
        } Toast.makeText(this, "Seleccionó tomo foto", Toast.LENGTH_SHORT).show();
    }//end takePhoto

    private File createPhotoFile(int photo_request_code) throws IOException {
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File photoFile = File.createTempFile(
                new Date() + "_photo",
                ".jpg",
                storageDir
        );
        if (photo_request_code == PHOTO_REQUEST_CODE) {
            mPhotoPath = "file:" + photoFile.getAbsolutePath();
            mAbsolutePhotoPath = photoFile.getAbsolutePath();
        }

        return photoFile;
    }//end createPhotoFile

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK){
            try {
                mPhotoFile = null;
                mImageFile = FileUtil.from(this,data.getData());
                mImageViewField.setImageBitmap(BitmapFactory.decodeFile(mImageFile.getAbsolutePath()));
            }catch (Exception e){
                Log.d("Error", "se produjo un error" + e.getMessage());
                Toast.makeText(this, "Se produjo un error" + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }

        /**
         * SELECCION DE FOTOGRAFIA
         */
        if (requestCode == PHOTO_REQUEST_CODE && resultCode == RESULT_OK) {
            mImageFile = null;
            mPhotoFile = new File(mAbsolutePhotoPath);
            Picasso.with(MantenedorCampoActivity.this).load(mPhotoPath).into(mImageViewField);
        }
    }//end onActivityResult
}
