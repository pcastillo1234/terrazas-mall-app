package com.terrazas.terrazasmallap.fragments;

import android.app.AlertDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.activities.MantenedorCategoryActivity;
import com.terrazas.terrazasmallap.models.Tac;
import com.terrazas.terrazasmallap.providers.AuthProvider;
import com.terrazas.terrazasmallap.providers.TacProvider;

import dmax.dialog.SpotsDialog;

public class DialogTermsAndConditions extends DialogFragment implements View.OnClickListener {
    Button btnOk;
    ImageButton ivClose;
    EditText edtMessage;
    String mMessage;
    AuthProvider mAuthProvider;
    TacProvider mTacProvider;
    Tac tac;
    AlertDialog mAlertDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_terms_and_conditions,container,false);
        btnOk = view.findViewById(R.id.btnUpdateTermsAndConditions);
        ivClose = view.findViewById(R.id.ivCloseTAC);
        edtMessage = view.findViewById(R.id.edtMessageTac);
        mAuthProvider = new AuthProvider();
        mTacProvider = new TacProvider();

        dataform();

        mAlertDialog = new SpotsDialog.Builder()
                .setContext(getContext())
                .setMessage("Espere un momento . . .")
                .setCancelable(false)
                .build();

        btnOk.setOnClickListener(this);
        ivClose.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        if(view == btnOk){
                updateTac();
        }
        else if(view == ivClose){
            dismiss();
        }
    }

    private void dataform() {
        tac = new Tac();
        mTacProvider.getData().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                QuerySnapshot query = task.getResult();
                if(!query.isEmpty()){
                    Log.d("Document data","Document data: "+ query.toString());
                    tac = query.getDocuments().get(0).toObject(Tac.class);
                    edtMessage.setText(tac.getDescription());
                }else{
                    createTac();
                }
            }
        });
    }

    private void updateTac() {
        mAlertDialog.show();
        mMessage = edtMessage.getText().toString();
        Tac updateTac = new Tac();
        updateTac.setId(mAuthProvider.getEmail());
        updateTac.setDescription(mMessage);
        mTacProvider.update(updateTac).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                mAlertDialog.dismiss();
                if (task.isSuccessful()){
                    dismiss();
                    Toast.makeText(getContext(), "Actualizado correctamente", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getContext(), "Error al actualizar", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void createTac() {
        mMessage = edtMessage.getText().toString();
        Tac newTac = new Tac();
        newTac.setId("terrazashco@hotmail.com");
        newTac.setDescription(mMessage);
        mTacProvider.save(newTac).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    Log.d("Status create","Tac successfully created");
                }else{
                    Log.d("Status create", "error creating Tac");
                }
            }
        });
    }

    public void validarCorreo(){
        if(!mAuthProvider.getEmail().equals("terrazashco@hotmail.com")){
            //
            edtMessage.setEnabled(false);
            edtMessage.setTextColor(Color.BLACK);
            //
            btnOk.setEnabled(false);
            btnOk.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        validarCorreo();
    }
}
