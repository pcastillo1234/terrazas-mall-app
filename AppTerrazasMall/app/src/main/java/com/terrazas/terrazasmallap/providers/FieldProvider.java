package com.terrazas.terrazasmallap.providers;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.terrazas.terrazasmallap.models.Field;

public class FieldProvider {
    CollectionReference mCollection;

    public FieldProvider(){
        mCollection = FirebaseFirestore.getInstance().collection("Field");
    }

    public Task<Void> save(Field field){
        return mCollection.document(field.getId()).set(field);
    }

    public Task<Void> update(Field field){
        return mCollection.document(field.getId())
                .update("title",field.getTitle(),
                        "description",field.getDescription(),
                        "direction",field.getDirection(),
                        "price",field.getPrice(),
                        "image",field.getImage(),
                        "days",field.getDays(),
                        "timeInterval",field.getTimeInterval(),
                        "startTime",field.getStartTime(),
                        "endTime",field.getEndTime(),
                        "idUser",field.getIdUser());
    }

    public Task<Void> delete(String idField){
        return  mCollection.document(idField).delete();
    }

    public Query getAll(){
        return mCollection.orderBy("title", Query.Direction.DESCENDING);
    }
}
