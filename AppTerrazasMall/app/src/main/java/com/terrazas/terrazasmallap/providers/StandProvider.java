package com.terrazas.terrazasmallap.providers;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.terrazas.terrazasmallap.models.Stand;

public class StandProvider {
    CollectionReference mCollection;

    public StandProvider(){
        mCollection = FirebaseFirestore.getInstance().collection("Stands");
    }

    public Task<Void> save(Stand stand){
        return mCollection.document(stand.getId()).set(stand);
    }

    public Task<Void> update(Stand stand){
        return mCollection.document(stand.getId())
                .update("idUser",stand.getIdUser(),
                        "numberStand",stand.getNumberStand(),
                        "title",stand.getTitle(),
                        "description",stand.getDescription(),
                        "image",stand.getImage());
    }

    public Task<Void> delete(String idStand){
        return  mCollection.document(idStand).delete();
    }

    public Query getAll(String id){
       return mCollection.whereEqualTo("idCategory",id).orderBy("numberStand", Query.Direction.ASCENDING);
    }
}
