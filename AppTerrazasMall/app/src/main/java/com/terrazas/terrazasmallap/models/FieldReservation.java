package com.terrazas.terrazasmallap.models;

public class FieldReservation {
    private String id;
    private String idUser;
    private String name;
    private String dni;
    private String cellphone;
    private String idField;
    private String titleField;
    private String date;
    private String idSchedule;
    private String descriptionSchedule;
    private String amount;

    public FieldReservation() {
    }

    public FieldReservation(String id, String idUser, String name, String dni, String cellphone, String idField,String titleField, String date,String idSchedule,String descriptionSchedule, String amount) {
        this.id = id;
        this.idUser = idUser;
        this.name = name;
        this.dni = dni;
        this.cellphone = cellphone;
        this.idField = idField;
        this.titleField = titleField;
        this.date = date;
        this.idSchedule = idSchedule;
        this.descriptionSchedule = descriptionSchedule;
        this.amount = amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getIdField() {
        return idField;
    }

    public void setIdField(String idField) {
        this.idField = idField;
    }

    public String getTitleField() {
        return titleField;
    }

    public void setTitleField(String titleField) {
        this.titleField = titleField;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getIdSchedule() {
        return idSchedule;
    }

    public void setIdSchedule(String idSchedule) {
        this.idSchedule = idSchedule;
    }

    public String getDescriptionSchedule() {
        return descriptionSchedule;
    }

    public void setDescriptionSchedule(String descriptionSchedule) {
        this.descriptionSchedule = descriptionSchedule;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
