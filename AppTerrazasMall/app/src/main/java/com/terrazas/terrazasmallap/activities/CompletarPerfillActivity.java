package com.terrazas.terrazasmallap.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.models.User;
import com.terrazas.terrazasmallap.providers.AuthProvider;
import com.terrazas.terrazasmallap.providers.UserProvider;

import dmax.dialog.SpotsDialog;

public class CompletarPerfillActivity extends AppCompatActivity {

    TextInputEditText mTextInputUsername;
    Button mButtonConfirmar;
    AuthProvider mAuthProvider;
    UserProvider mUserProvider;
    AlertDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completar_perfil);
        mTextInputUsername = findViewById(R.id.txtUsername);
        mButtonConfirmar = findViewById(R.id.btnConfirm);

        mAuthProvider = new AuthProvider();
        mUserProvider = new UserProvider();
        mDialog = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Espere un momento . . .")
                .setCancelable(false)
                .build();


        mButtonConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });
    }

    private void register() {
        String username = mTextInputUsername.getText().toString();
        if(!username.isEmpty() ){
            updateUser(username);
        }else {
            Toast.makeText(this, "Complete los campos", Toast.LENGTH_LONG).show();
        }
    }



    private void updateUser(final String username ){
            String id = mAuthProvider.getUid();
            User user = new User();
            user.setUsername(username);
            user.setId(id);
            user.setDni("");
            user.setCellphone("");
            mDialog.show();
            mUserProvider.update(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    mDialog.dismiss();
                    if(task.isSuccessful()){
                        finish();
                        Intent intent = new Intent(CompletarPerfillActivity.this, HomeActivity.class);
                        startActivity(intent);
                    }
                    else{
                        Toast.makeText(CompletarPerfillActivity.this, "No se pudo almacenar el usuario en la base de datos", Toast.LENGTH_SHORT).show();
                     }
                }
            });


    }




}