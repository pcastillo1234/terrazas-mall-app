package com.terrazas.terrazasmallap.providers;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.terrazas.terrazasmallap.models.FieldReservation;

public class FieldReservationProvider {
    CollectionReference mCollection;

    public FieldReservationProvider(){
        mCollection = FirebaseFirestore.getInstance().collection("FieldReservation");
    }

    public Task<Void> save(FieldReservation fieldReservation){
        return mCollection.document(fieldReservation.getId()).set(fieldReservation);
    }

    public Task<QuerySnapshot> getDataForUser(String id){
        return mCollection.whereEqualTo("idUser",id).orderBy("date", Query.Direction.DESCENDING).get();
    }

    public Task<QuerySnapshot> getAll(){
        return mCollection.orderBy("date", Query.Direction.DESCENDING).get();
    }

    public Task<QuerySnapshot> getReservationForIDField(String idField,String date){
        return  mCollection.whereEqualTo("idField",idField).whereEqualTo("date",date).get();
    }
}
