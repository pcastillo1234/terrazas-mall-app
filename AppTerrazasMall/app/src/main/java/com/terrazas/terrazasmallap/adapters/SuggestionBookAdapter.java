package com.terrazas.terrazasmallap.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.fragments.ListSuggestionBookFragment;
import com.terrazas.terrazasmallap.fragments.PerfilFragment;
import com.terrazas.terrazasmallap.models.SuggestionBook;
import com.terrazas.terrazasmallap.providers.SuggestionBookProvider;

import java.util.List;

public class SuggestionBookAdapter extends ArrayAdapter<SuggestionBook> {

    TextView txtIssue;
    TextView txtMessage;
    TextView txtDate;
    ImageButton ivDelete;
    AlertDialog.Builder alertDialog;
    SuggestionBookProvider mSuggestionBookProvider;

    public SuggestionBookAdapter(Context context, List<SuggestionBook> object){
        super(context,0,object);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null){
            convertView = ((Activity)getContext()).getLayoutInflater().inflate(R.layout.listview_suggestion_book,parent,false);
        }
        txtIssue = convertView.findViewById(R.id.txtIssueList);
        txtMessage = convertView.findViewById(R.id.txtMessageList);
        txtDate = convertView.findViewById(R.id.txtDateList);
        ivDelete = convertView.findViewById(R.id.ivDeleteList);
        mSuggestionBookProvider = new SuggestionBookProvider();

        SuggestionBook suggestionBook = getItem(position);

        txtIssue.setText(suggestionBook.getIssue());
        txtMessage.setText(suggestionBook.getMessage());
        txtDate.setText(suggestionBook.getDate());

        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog = new AlertDialog.Builder(view.getContext());
                alertDialog.setMessage("¿Realmente desea eliminar la Sugerencia "+suggestionBook.getIssue()+"?")
                        .setCancelable(true)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                mSuggestionBookProvider.delete(suggestionBook.getId())
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if(task.isSuccessful()){
                                                    AppCompatActivity appCompatActivity = (AppCompatActivity)view.getContext();
                                                    appCompatActivity.getSupportFragmentManager().beginTransaction()
                                                            .replace(R.id.container,new ListSuggestionBookFragment())
                                                            .addToBackStack(null)
                                                            .commit();
                                                    Toast.makeText(view.getContext(),"Sugerencia eliminada correctamente",Toast.LENGTH_SHORT).show();
                                                }else{
                                                    Toast.makeText(view.getContext(),"Error al eliminar la Sugerencia",Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                        .create()
                        .show();
            }//end onClick
        });

        return convertView;
    }
}
