package com.terrazas.terrazasmallap.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.models.Product;
import com.terrazas.terrazasmallap.providers.AuthProvider;
import com.terrazas.terrazasmallap.providers.ImageProvider;
import com.terrazas.terrazasmallap.providers.ProductProvider;
import com.terrazas.terrazasmallap.utils.FileUtil;


import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;

public class MaintainerProductActivity extends AppCompatActivity {

    ImageView mImageViewProduct;
    private final int GALLERY_REQUEST_CODE = 1;
    private final int PHOTO_REQUEST_CODE = 2;
    File mImageFile;
    Button mButtonProduct;
    Button mButtonUpdateProduct;
    ImageProvider mImageProvider;
    TextInputEditText mTextInputTitle;
    TextInputEditText mTextInputPrice;
    CircleImageView mCircleImageBack;
    ProductProvider mProductProvider;
    String mTittle = "";
    String mPrice = "";
    String mIDStand = "";
    String idProduct;
    String image;
    Boolean statusBtnMain;
    AuthProvider mAutProvider;
    AlertDialog mAlertDialog;
    AlertDialog.Builder mBuilderSelector;
    CharSequence options[];

    // photo 1
    String mAbsolutePhotoPath;
    String mPhotoPath;
    File mPhotoFile;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_mantenedor_product);
        mTextInputTitle = findViewById(R.id.txtTitleProduct);
        mTextInputPrice = findViewById(R.id.txtPriceProduct);
        mImageViewProduct = findViewById(R.id.imageViewProduct1);
        mImageProvider = new ImageProvider();
        mProductProvider = new ProductProvider();
        mAutProvider = new AuthProvider();
        mButtonProduct = findViewById(R.id.btnProduct);
        mButtonUpdateProduct = findViewById(R.id.btnUpdateProduct);
        mCircleImageBack = findViewById(R.id.circleImageBackProduct);

        Intent intent = getIntent();
        mIDStand = intent.getStringExtra("idStand");
        idProduct = intent.getStringExtra("idProduct");
        statusBtnMain = intent.getBooleanExtra("status",true);
        image = intent.getStringExtra("image");
        statusButton(statusBtnMain);

        mCircleImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mAlertDialog = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Espere un momento . . .")
                .setCancelable(false)
                .build();
        mBuilderSelector = new AlertDialog.Builder(this);
        mBuilderSelector.setTitle("Selecciona una opción");
        options = new  CharSequence[] {"Imagen de galeria" , "Tomar Foto"};

        mImageViewProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectOptionImage(1);
            }
        });

        mButtonProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickProduct();
            }
        });

        mButtonUpdateProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProduct();
            }
        });
    }//end onCreate

    private void statusButton(Boolean statusButtonMaintainer) {
        if(!statusButtonMaintainer){
            this.mButtonProduct.setEnabled(false);
            this.mButtonProduct.setVisibility(View.INVISIBLE);
            this.mButtonUpdateProduct.setEnabled(true);
            this.mButtonUpdateProduct.setVisibility(View.VISIBLE);
            this.mTextInputTitle.setText(getIntent().getStringExtra("title"));
            this.mTextInputPrice.setText(getIntent().getStringExtra("price"));
            Picasso.with(MaintainerProductActivity.this).load(image).into(this.mImageViewProduct);
        }
    }//end statusButton

    private void updateProduct() {
        mTittle = mTextInputTitle.getText().toString();
        mPrice = mTextInputPrice.getText().toString();
        if(!mTittle.isEmpty() && !mPrice.isEmpty()){
            if(mImageFile!=null){
                updateImage2(mImageFile );
            }
            else if (mPhotoFile != null ) {
                updateImage2(mPhotoFile);
            }
            else{
                updateImage1();
            }
        }else if(mTittle.isEmpty()){
            if(mPrice.isEmpty()){
                mTextInputTitle.setError("Campo obligatorio");
                mTextInputPrice.setError("Campo obligatorio");
                mTextInputTitle.requestFocus();
            }else{
                mTextInputTitle.setError("Campo obligatorio");
                mTextInputTitle.requestFocus();
            }
        }else if(mPrice.isEmpty()){
            mTextInputPrice.setError("Campo obligatorio");
            mTextInputPrice.requestFocus();
        }
    }//end updateProduct

    private void updateImage1() {
        mAlertDialog.show();
        Product product = new Product();
        product.setImage(image);
        product.setId(idProduct);
        product.setTitle(mTittle);
        product.setPrice(mPrice);
        product.setIdUser(mAutProvider.getUid());
        product.setIdStand(mIDStand);
        mProductProvider.update(product).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> taskSave) {
                mAlertDialog.dismiss();
                if (taskSave.isSuccessful()){
                    finish();
                    Toast.makeText(MaintainerProductActivity.this, "Se actualizó correctamente", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(MaintainerProductActivity.this, "No se pudo actualizar la información", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }//end updateImage1

    private void updateImage2(File imageFile1) {
        mAlertDialog.show();
        mImageProvider.save(MaintainerProductActivity.this,imageFile1).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()){
                    mImageProvider.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            final String url = uri.toString();
                            Product product = new Product();
                            product.setImage(url);
                            product.setId(idProduct);
                            product.setTitle(mTittle);
                            product.setPrice(mPrice);
                            product.setIdUser(mAutProvider.getUid());
                            product.setIdStand(mIDStand);
                            mProductProvider.update(product).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> taskSave) {
                                    mAlertDialog.dismiss();
                                    if (taskSave.isSuccessful()){
                                        finish();
                                        Toast.makeText(MaintainerProductActivity.this, "Se actualizó correctamente", Toast.LENGTH_SHORT).show();
                                    }else{
                                        Toast.makeText(MaintainerProductActivity.this, "No se pudo actualizar la información", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                    });
                }else{
                    mAlertDialog.dismiss();
                    Toast.makeText(MaintainerProductActivity.this, "Error al actualizar la imagen", Toast.LENGTH_LONG).show();
                }
            }
        });
    }//end updateImage2

    private void clickProduct() {
        mTittle = mTextInputTitle.getText().toString();
        mPrice = mTextInputPrice.getText().toString();
        if(!mTittle.isEmpty() && !mPrice.isEmpty()){
            if(mImageFile!=null){
                saveImage(mImageFile );
            }
            else if (mPhotoFile != null ) {
                saveImage(mPhotoFile);
            }
            else{
                Toast.makeText(this, "Debes seleccionar una imagen", Toast.LENGTH_SHORT).show();
            }
        }else if(mTittle.isEmpty()){
            if(mPrice.isEmpty()){
                mTextInputTitle.setError("Campo obligatorio");
                mTextInputPrice.setError("Campo obligatorio");
                mTextInputTitle.requestFocus();
            }else{
                mTextInputTitle.setError("Campo obligatorio");
                mTextInputTitle.requestFocus();
            }
        }else if(mPrice.isEmpty()){
            mTextInputPrice.setError("Campo obligatorio");
            mTextInputPrice.requestFocus();
        }
    }//end clickProduct

    private void saveImage(File imageFile1) {
        mAlertDialog.show();
        mImageProvider.save(MaintainerProductActivity.this,imageFile1).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()){
                    mImageProvider.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            final String url = uri.toString();
                            Product product = new Product();
                            product.setImage(url);
                            product.setId(UUID.randomUUID().toString());
                            product.setTitle(mTittle);
                            product.setPrice(mPrice);
                            product.setIdUser(mAutProvider.getUid());
                            product.setIdStand(mIDStand);
                            mProductProvider.save(product).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> taskSave) {
                                    mAlertDialog.dismiss();
                                    if (taskSave.isSuccessful()){
                                        //clearForm();
                                        finish();
                                        Toast.makeText(MaintainerProductActivity.this, "Se almacenó correctamente", Toast.LENGTH_SHORT).show();
                                    }else{
                                        Toast.makeText(MaintainerProductActivity.this, "No se pudo almacenar la información", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                    });
                }else{
                    mAlertDialog.dismiss();
                    Toast.makeText(MaintainerProductActivity.this, "Error al almacenar la imagen", Toast.LENGTH_LONG).show();
                }
            }
        });
    }//end saveImage

    private void clearForm() {
        mTextInputTitle.setText("");
        mTextInputPrice.setText("");
        mImageViewProduct.setImageResource(R.drawable.ic_camera_24);
        mTittle =  "";
        mPrice = "";
        mImageFile = null;
    }//end clearForm

    private void selectOptionImage(final int numberImage) {
        mBuilderSelector.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == 0) {
                    if (numberImage == 1) {
                        openGallery(GALLERY_REQUEST_CODE);
                    }
                }
                else if (i == 1){
                    if (numberImage == 1) {
                        takePhoto(PHOTO_REQUEST_CODE);
                    }
                }
            }
        });
        mBuilderSelector.show();
    }//end selectOptionImage

    private void openGallery(int requestCode) {
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent,requestCode);
    }//end openGallery

    private void takePhoto(int requestCode) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createPhotoFile(requestCode);
            } catch(Exception e) {
                Toast.makeText(this, "Hubo un error con el archivo " + e.getMessage(), Toast.LENGTH_LONG).show();
            }

            if (photoFile != null) {
                Uri photoUri = FileProvider.getUriForFile(MaintainerProductActivity.this, "com.terrazas.terrazasmallap", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(takePictureIntent, requestCode);
            }
        } Toast.makeText(this, "Seleccionó tomo foto", Toast.LENGTH_SHORT).show();
    }//end takePhoto

    private File createPhotoFile(int requestCode) throws IOException {
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File photoFile = File.createTempFile(
                new Date() + "_photo",
                ".jpg",
                storageDir
        );
        if (requestCode == PHOTO_REQUEST_CODE) {
            mPhotoPath = "file:" + photoFile.getAbsolutePath();
            mAbsolutePhotoPath = photoFile.getAbsolutePath();
        }

        return photoFile;
    }//end createPhotoFile

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK){
            try {
                mPhotoFile = null;
                mImageFile = FileUtil.from(this,data.getData());
                mImageViewProduct.setImageBitmap(BitmapFactory.decodeFile(mImageFile.getAbsolutePath()));
            }catch (Exception e){
                Log.d("Error", "se produjó un error" + e.getMessage());
                Toast.makeText(this, "Se produjó un error" + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }

        /**
         * SELECCION DE FOTOGRAFIA
         */
        if (requestCode == PHOTO_REQUEST_CODE && resultCode == RESULT_OK) {
            mImageFile = null;
            mPhotoFile = new File(mAbsolutePhotoPath);
            Picasso.with(MaintainerProductActivity.this).load(mPhotoPath).into(mImageViewProduct);
        }
    }//end onActivityResult
}
