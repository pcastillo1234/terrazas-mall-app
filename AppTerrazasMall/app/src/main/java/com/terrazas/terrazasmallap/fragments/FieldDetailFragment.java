package com.terrazas.terrazasmallap.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;
import com.terrazas.terrazasmallap.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class FieldDetailFragment extends Fragment {

    View mView;
    String idField;
    String image;
    String direction;
    String description;
    String title;
    String price;
    String timeInterval;
    String startTime;
    String endTime;
    String days;

    ImageView imageView;
    TextView txtPrice;
    TextView txtDays;
    TextView txtTitle;
    TextView txtDirection;
    TextView txtDescription;
    TextView txtSchedule;
    CircleImageView circleImageView;
    Button btnReservation;

    public FieldDetailFragment() {
    }

    public FieldDetailFragment(String idField,String image,String direction,String description,String title,String price,String timeInterval,String startTime,String endTime,String days) {
        this.idField = idField;
        this.image = image;
        this.direction = direction;
        this.description = description;
        this.title = title;
        this.price = price;
        this.timeInterval = timeInterval;
        this.startTime = startTime;
        this.endTime = endTime;
        this.days = days;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_field_detail,container,false);
        imageView = mView.findViewById(R.id.ivFDF);
        txtPrice = mView.findViewById(R.id.lblPriceFDF);
        txtDays = mView.findViewById(R.id.lblDaysFDF);
        txtTitle = mView.findViewById(R.id.lblTitleFDF);
        txtDirection = mView.findViewById(R.id.lblDirectionFDF);
        txtDescription = mView.findViewById(R.id.lblDescriptionFDF);
        txtSchedule = mView.findViewById(R.id.lblScheduleFDF);
        circleImageView = mView.findViewById(R.id.circleImageBackFDF);
        btnReservation = mView.findViewById(R.id.btnReservationACD);

        txtPrice.setText("S/"+price+" ("+timeInterval+" minutos)");
        txtSchedule.setText(startTime+" "+getAM_PM(startTime)+" - "+endTime+" "+getAM_PM(endTime));
        txtDays.setText(days);
        txtTitle.setText(title);
        txtDirection.setText(direction);
        txtDescription.setText(description);
        if(image != null){
            if(!image.isEmpty()){
                Picasso.with(getContext()).load(image).into(imageView);
            }
        }

        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPress();
            }
        });

        btnReservation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToReservation();
            }
        });

        return mView;
    }//end onCreateView

    private String getAM_PM(@NonNull String time) {
        String[] a = time.split(":");
        int a0 = Integer.parseInt(a[0]);
        String AM_PM;
        if(a0 < 12) {
            AM_PM = "a.m.";
        } else {
            AM_PM = "p.m.";
        }
        return AM_PM;
    }

    private void goToReservation() {
        AppCompatActivity activity = (AppCompatActivity)getContext();
        activity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.container,new SportFieldReservationFragment(idField,title,price,timeInterval,startTime,endTime))
                .addToBackStack(null)
                .commit();
    }//end goToReservation

    public void onBackPress(){
        AppCompatActivity activity = (AppCompatActivity)getContext();
        activity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.container,new CampoFragment())
                .addToBackStack(null)
                .commit();
    }//end onBackPress
}
