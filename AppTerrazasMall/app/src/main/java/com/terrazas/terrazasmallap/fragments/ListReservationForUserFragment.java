package com.terrazas.terrazasmallap.fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.adapters.ReservationListAdapter;
import com.terrazas.terrazasmallap.models.FieldReservation;
import com.terrazas.terrazasmallap.providers.AuthProvider;
import com.terrazas.terrazasmallap.providers.FieldReservationProvider;

import java.util.ArrayList;
import java.util.List;

public class ListReservationForUserFragment extends Fragment {

    ImageButton ivBack;
    ListView mMyReservationList;
    FieldReservationProvider mFieldReservationProvider;
    AuthProvider mAuthProvider;
    ReservationListAdapter mReservationListAdapter;
    List<FieldReservation> mList;

    public ListReservationForUserFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_reservation_for_user,container,false);
        ivBack = view.findViewById(R.id.ivBack);
        mMyReservationList = view.findViewById(R.id.MyReservationList);
        mFieldReservationProvider = new FieldReservationProvider();
        mAuthProvider = new AuthProvider();

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                returnProfile();
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mFieldReservationProvider.getDataForUser(mAuthProvider.getUid()).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                mList = new ArrayList<>();
                if(task.isSuccessful()){
                    for(QueryDocumentSnapshot document : task.getResult()){
                        FieldReservation mData = document.toObject(FieldReservation.class);
                        mList.add(mData);
                    }//end for
                    mReservationListAdapter = new ReservationListAdapter(getContext(),mList);
                    mMyReservationList.setAdapter(mReservationListAdapter);
                }else{
                    Log.d("Error","Error getting documents: ",task.getException());
                }
            }
        });
    }

    private void returnProfile() {
        AppCompatActivity appCompatActivity = (AppCompatActivity)this.getContext();
        appCompatActivity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.container,new PerfilFragment())
                .addToBackStack(null)
                .commit();
    }

}
