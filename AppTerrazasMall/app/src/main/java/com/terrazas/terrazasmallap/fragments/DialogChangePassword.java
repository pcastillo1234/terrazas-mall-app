package com.terrazas.terrazasmallap.fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.activities.MainActivity;
import com.terrazas.terrazasmallap.providers.AuthProvider;

import dmax.dialog.SpotsDialog;

public class DialogChangePassword extends DialogFragment implements View.OnClickListener {
    Button btnOk;
    ImageButton ivClose;
    ImageView ivVisibilityOldPass;
    ImageView ivVisibilityNewPass;
    ImageView ivVisibilityRepeatNewPass;
    EditText edtOldPass;
    EditText edtNewPass;
    EditText edtRepeatNewPass;

    Boolean ivOldPass = false;
    Boolean ivNewPass = false;
    Boolean ivRepeatNewPass = false;

    String mOldPass;
    String mNewPass;
    String mRepeatNewPass;

    AuthProvider mAuthProvider;
    AlertDialog mAlertDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_change_password,container,false);
        btnOk = view.findViewById(R.id.btnUpdatePassword);
        ivClose = view.findViewById(R.id.ivCloseCP);
        ivVisibilityOldPass = view.findViewById(R.id.ivVisibility_old_pass);
        ivVisibilityNewPass = view.findViewById(R.id.ivVisibility_new_password);
        ivVisibilityRepeatNewPass = view.findViewById(R.id.ivVisibility_repeat_new_password);
        edtOldPass = view.findViewById(R.id.edtOldPassword);
        edtNewPass = view.findViewById(R.id.edtNewPassword);
        edtRepeatNewPass = view.findViewById(R.id.edtRepeatNewPassword);
        mAuthProvider = new AuthProvider();

        mAlertDialog = new SpotsDialog.Builder()
                .setContext(getContext())
                .setMessage("Espere un momento . . .")
                .setCancelable(false)
                .build();

        btnOk.setOnClickListener(this);
        ivClose.setOnClickListener(this);
        ivVisibilityOldPass.setOnClickListener(this);
        ivVisibilityNewPass.setOnClickListener(this);
        ivVisibilityRepeatNewPass.setOnClickListener(this);

        return  view;
    }

    @Override
    public void onClick(View view) {
        if(view == btnOk){
            changepass();
        }
        else if(view == ivVisibilityOldPass){
            if(!ivOldPass){
                edtOldPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                edtOldPass.setSelection(edtOldPass.getText().length());
                ivVisibilityOldPass.setBackground(view.getContext().getDrawable(R.drawable.ic_baseline_visibility_24));
                ivOldPass = true;
            }else{
                edtOldPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                edtOldPass.setSelection(edtOldPass.getText().length());
                ivVisibilityOldPass.setBackground(view.getContext().getDrawable(R.drawable.ic_baseline_visibility_off_24));
                ivOldPass = false;
            }
        }
        else if(view == ivVisibilityNewPass){
            if(!ivNewPass){
                edtNewPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                edtNewPass.setSelection(edtNewPass.getText().length());
                ivVisibilityNewPass.setBackground(view.getContext().getDrawable(R.drawable.ic_baseline_visibility_24));
                ivNewPass = true;
            }else{
                edtNewPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                edtNewPass.setSelection(edtNewPass.getText().length());
                ivVisibilityNewPass.setBackground(view.getContext().getDrawable(R.drawable.ic_baseline_visibility_off_24));
                ivNewPass = false;
            }
        }
        else if(view == ivVisibilityRepeatNewPass){
            if(!ivRepeatNewPass){
                edtRepeatNewPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                edtRepeatNewPass.setSelection(edtRepeatNewPass.getText().length());
                ivVisibilityRepeatNewPass.setBackground(view.getContext().getDrawable(R.drawable.ic_baseline_visibility_24));
                ivRepeatNewPass = true;
            }else{
                edtRepeatNewPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                edtRepeatNewPass.setSelection(edtRepeatNewPass.getText().length());
                ivVisibilityRepeatNewPass.setBackground(view.getContext().getDrawable(R.drawable.ic_baseline_visibility_off_24));
                ivRepeatNewPass = false;
            }
        }
        else if(view == ivClose){
            dismiss();
        }
    }//end onClick

    public void changepass(){
        mOldPass = edtOldPass.getText().toString();
        mNewPass = edtNewPass.getText().toString();
        mRepeatNewPass = edtRepeatNewPass.getText().toString();
        if(!mOldPass.isEmpty() && mNewPass.length()>5 && !mRepeatNewPass.isEmpty()){
            mAlertDialog.show();
            mAuthProvider.reauthenticate(mAuthProvider.getEmail(),mOldPass).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        mAlertDialog.dismiss();
                        if(mNewPass.equals(mRepeatNewPass)){
                            mAuthProvider.updatePass(mNewPass).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    mAlertDialog.dismiss();
                                    if(task.isSuccessful()){
                                        dismiss();
                                        Toast.makeText(getContext(),"Contraseña actualizada correctamente",Toast.LENGTH_SHORT).show();
                                    }else{
                                        mAlertDialog.dismiss();
                                        Toast.makeText(getContext(),"Error al actualizar contraseña",Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }else{
                            mAlertDialog.dismiss();
                            edtRepeatNewPass.setError("Las contraseñas no coinciden");
                            edtRepeatNewPass.requestFocus();
                        }
                    }else{
                        mAlertDialog.dismiss();
                        edtOldPass.setError("Contraseña incorrecta");
                        edtOldPass.requestFocus();
                    }
                }
            });
        }else if(mOldPass.isEmpty()){
            if(mNewPass.isEmpty() && mRepeatNewPass.isEmpty()){
                edtOldPass.setError("Campo obligatorio");
                edtNewPass.setError("Campo obligatorio");
                edtRepeatNewPass.setError("Campo obligatorio");
                edtOldPass.requestFocus();
            }else{
                edtOldPass.setError("Campo obligatorio");
                edtOldPass.requestFocus();
            }
        }else if(mNewPass.length() <=5){
            if(mRepeatNewPass.isEmpty()){
                edtNewPass.setError("Mínimo 6 caracteres");
                edtRepeatNewPass.setError("Campo obligatorio");
                edtNewPass.requestFocus();
            }else{
                edtNewPass.setError("Mínimo 6 caracteres");
                edtNewPass.requestFocus();
            }
        }else if(mRepeatNewPass.isEmpty()){
                edtRepeatNewPass.setError("Campo obligatorio");
                edtRepeatNewPass.requestFocus();
        }
    }//end change pass

}
