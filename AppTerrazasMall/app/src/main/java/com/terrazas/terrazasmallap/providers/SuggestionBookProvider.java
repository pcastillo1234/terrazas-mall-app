package com.terrazas.terrazasmallap.providers;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.terrazas.terrazasmallap.models.SuggestionBook;

public class SuggestionBookProvider {
    CollectionReference mCollection;

    public SuggestionBookProvider(){
        mCollection = FirebaseFirestore.getInstance().collection("SuggestionBook");
    }

    public Task<Void> save(SuggestionBook suggestionBook){
        return mCollection.document(suggestionBook.getId()).set(suggestionBook);
    }

    public Task<Void> delete(String idSuggestionBook){
        return  mCollection.document(idSuggestionBook).delete();
    }

    public Task<QuerySnapshot> getAll(){
        return mCollection.orderBy("date", Query.Direction.DESCENDING).get();
    }
}
