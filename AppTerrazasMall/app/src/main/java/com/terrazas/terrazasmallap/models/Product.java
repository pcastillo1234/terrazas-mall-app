package com.terrazas.terrazasmallap.models;

public class Product {
    private String id;
    private String title;
    private String price;
    private String image;
    private String idStand;
    private String idUser;

    public Product() {
    } //needed for firebase

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIdStand() {
        return idStand;
    }

    public void setIdStand(String idStand) {
        this.idStand = idStand;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }
}
