package com.terrazas.terrazasmallap.models;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.util.List;
import java.util.Locale;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.biff.FontRecord;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.Orientation;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;


public class ExcelExportSuggestionBook {

    public static void export(List<SuggestionBook> mList){
        File fileSD = Environment.getExternalStorageDirectory();
        String fileName = "Libro de Sugerencias.xls";
        File directory = new File(fileSD.getAbsolutePath());

        if(!directory.isDirectory()){
            directory.mkdirs();
        }
        try{
            File file = new File(directory,fileName);
            WorkbookSettings workbookSettings = new WorkbookSettings();
            workbookSettings.setLocale(new Locale(Locale.ENGLISH.getLanguage(),Locale.ENGLISH.getCountry()));
            WritableWorkbook workbook;
            workbook = Workbook.createWorkbook(file,workbookSettings);

            //Header
            WritableFont writableFontHeader = new WritableFont(WritableFont.createFont("Arial"), WritableFont.DEFAULT_POINT_SIZE, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.WHITE);
            WritableCellFormat cellFormatHeader = new WritableCellFormat(writableFontHeader);
            cellFormatHeader.setAlignment(Alignment.CENTRE);
            cellFormatHeader.setVerticalAlignment(VerticalAlignment.CENTRE);
            cellFormatHeader.setBorder(Border.ALL, BorderLineStyle.THICK, Colour.BLACK);
            cellFormatHeader.setBackground(Colour.GRAY_80);

            //Body
            WritableCellFormat cellFormatBody = new WritableCellFormat();
            cellFormatBody.setAlignment(Alignment.CENTRE);
            cellFormatBody.setVerticalAlignment(VerticalAlignment.CENTRE);
            cellFormatBody.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
            cellFormatBody.setWrap(true);

            //Body Message
            WritableCellFormat cellFormatBodyMessage = new WritableCellFormat();
            cellFormatBodyMessage.setAlignment(Alignment.JUSTIFY);
            cellFormatBodyMessage.setVerticalAlignment(VerticalAlignment.CENTRE);
            cellFormatBodyMessage.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
            cellFormatBodyMessage.setWrap(true);

            WritableSheet sheet1 = workbook.createSheet("Libro de Sugerencias",0);
            sheet1.addCell(new Label(0,0,"FECHA",cellFormatHeader));
            sheet1.addCell(new Label(1,0,"ASUNTO",cellFormatHeader));
            sheet1.addCell(new Label(2,0,"MENSAJE",cellFormatHeader));

            int cont = 1;
            for(SuggestionBook suggestionBook : mList){
                sheet1.addCell(new Label(0,cont,suggestionBook.getDate(),cellFormatBody));
                sheet1.addCell(new Label(1,cont,suggestionBook.getIssue(),cellFormatBody));
                sheet1.addCell(new Label(2,cont,suggestionBook.getMessage(),cellFormatBodyMessage));
                cont++;
                Log.d("SuggestionBook Data", suggestionBook.getDate()+" | "+suggestionBook.getIssue()+" | "+suggestionBook.getMessage());
            }
            sheet1.setColumnView(0,12);
            sheet1.setColumnView(1,30);
            sheet1.setColumnView(2,50);
            workbook.write();
            workbook.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }//end export
}
