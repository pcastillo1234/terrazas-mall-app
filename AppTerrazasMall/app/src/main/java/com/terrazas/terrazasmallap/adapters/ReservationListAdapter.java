package com.terrazas.terrazasmallap.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.models.FieldReservation;


import java.util.List;

public class ReservationListAdapter extends ArrayAdapter<FieldReservation> {

    TextView txtName;
    TextView txtDate;
    TextView txtTitleField;
    TextView txtSchedule;
    TextView txtAmaount;

    public ReservationListAdapter(Context context, List<FieldReservation> object){
        super(context,0,object);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null){
            convertView = ((Activity)getContext()).getLayoutInflater().inflate(R.layout.listview_reservation,parent,false);
        }
        txtName = convertView.findViewById(R.id.txtNameLR);
        txtDate = convertView.findViewById(R.id.txtDateLR);
        txtTitleField = convertView.findViewById(R.id.txtTitleFieldLR);
        txtSchedule = convertView.findViewById(R.id.txtScheduleLR);
        txtAmaount = convertView.findViewById(R.id.txtAmountLR);

        FieldReservation fieldReservation = getItem(position);
        txtName.setText(fieldReservation.getName());
        txtDate.setText(fieldReservation.getDate());
        txtTitleField.setText(fieldReservation.getTitleField());
        txtSchedule.setText("Horario:\n"+fieldReservation.getDescriptionSchedule());
        txtAmaount.setText("S/"+fieldReservation.getAmount());

        return convertView;
    }

}
