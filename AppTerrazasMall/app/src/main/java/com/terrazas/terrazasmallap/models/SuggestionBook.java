package com.terrazas.terrazasmallap.models;

import java.util.Date;
import java.util.Map;

public class SuggestionBook {

    private String id;
    private String issue;
    private String message;
    private String date;
    private String idUser;

    public SuggestionBook() {
    }

    public SuggestionBook(String id, String issue, String message, String date, String idUser) {
        this.id = id;
        this.issue = issue;
        this.message = message;
        this.date = date;
        this.idUser = idUser;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }
}
