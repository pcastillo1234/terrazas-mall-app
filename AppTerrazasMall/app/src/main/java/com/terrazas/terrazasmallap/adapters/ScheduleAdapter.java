package com.terrazas.terrazasmallap.adapters;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.models.Schedule;

import java.util.List;

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.ViewHolder> {

    List<Schedule> scheduleList;
    List<Schedule> selectedScheduleList;
    View mView;
    String code="";

    public ScheduleAdapter(List<Schedule> scheduleList, List<Schedule> selectedScheduleList) {
        this.scheduleList = scheduleList;
        this.selectedScheduleList = selectedScheduleList;
    }

    @Override
    public int getItemCount() {
        return scheduleList.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_schedule,parent,false);
        return new  ScheduleAdapter.ViewHolder(mView);
    }//end onCreateViewHolder

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtSchedule.setText(scheduleList.get(position).getTime()+" "+getAM_PM(scheduleList.get(position).getTime()));
        for(Schedule selectedSchedule: selectedScheduleList){
            if(scheduleList.get(position).getIdSchedule().equals(selectedSchedule.getIdSchedule())){
                //Disable cardView
                holder.mCardView.setCardBackgroundColor(Color.RED);
                holder.txtSchedule.setTextColor(Color.WHITE);
                holder.mCardView.setEnabled(false);
            }
        }//end for
        holder.mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!holder.colorCardView){
                    holder.mCardView.setCardBackgroundColor(Color.GREEN);
                    if(code.isEmpty()){
                        code += scheduleList.get(position).getIdSchedule();
                    }else{
                        code += ","+scheduleList.get(position).getIdSchedule();
                    }
                    holder.colorCardView = true;
                }else{
                    holder.mCardView.setCardBackgroundColor(Color.WHITE);
                    String[] codeChart = code.split(",");
                    String newCode = "";
                    for (int i=0;i<codeChart.length;i++){
                        if(codeChart[i].equals(scheduleList.get(position).getIdSchedule())){
                            codeChart[i] = "";
                        }
                        if(newCode.isEmpty()){
                            newCode +=codeChart[i];
                        }else if(!codeChart[i].isEmpty()){
                            newCode += ","+codeChart[i];
                        }
                    }
                    code = newCode;
                    holder.colorCardView = false;
                }
                //code += scheduleList.get(position).getIdSchedule();
                Log.d("Code schedule",code);
            }
        });
    }//end onBindViewHolder

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView txtSchedule;
        CardView mCardView;
        Boolean colorCardView = false;
        public ViewHolder(View view){
            super(view);
            txtSchedule = view.findViewById(R.id.txtTimeCVSchedule);
            mCardView = view.findViewById(R.id.cardViewSchedule);
        }
    }//end viewHolder

    public String getCode(){
        return code;
    }

    private String getAM_PM(@NonNull String time) {
        String[] a = time.split(":");
        int a0 = Integer.parseInt(a[0]);
        String AM_PM;
        if(a0 < 12) {
            AM_PM = "a.m.";
        } else {
            AM_PM = "p.m.";
        }
        return AM_PM;
    }
}
