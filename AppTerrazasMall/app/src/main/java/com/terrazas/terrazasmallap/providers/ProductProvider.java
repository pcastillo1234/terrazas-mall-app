package com.terrazas.terrazasmallap.providers;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.terrazas.terrazasmallap.models.Product;

public class ProductProvider {
    CollectionReference mCollection;

    public ProductProvider(){
        mCollection = FirebaseFirestore.getInstance().collection("Product");
    }

    public Task<Void> save(Product product){
        return mCollection.document(product.getId()).set(product);
    }

    public Task<Void> update(Product product){
        return mCollection.document(product.getId())
                .update("idUser",product.getIdUser(),
                        "title",product.getTitle(),
                        "price",product.getPrice(),
                        "image",product.getImage());
    }

    public Task<Void> delete(String idProduct){
        return  mCollection.document(idProduct).delete();
    }

    public Query getAll(String id){
        return mCollection.whereEqualTo("idStand",id).orderBy("title", Query.Direction.DESCENDING);
    }
}
