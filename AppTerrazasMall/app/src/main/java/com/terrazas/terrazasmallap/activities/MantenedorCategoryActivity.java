package com.terrazas.terrazasmallap.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.models.Category;
import com.terrazas.terrazasmallap.providers.AuthProvider;
import com.terrazas.terrazasmallap.providers.CategoryProvider;
import com.terrazas.terrazasmallap.providers.ImageProvider;
import com.terrazas.terrazasmallap.utils.FileUtil;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;

public class MantenedorCategoryActivity extends AppCompatActivity {

    ImageView mImageViewCategory;
    private final int GALLERY_REQUEST_CODE = 1;
    private final int PHOTO_REQUEST_CODE = 2;
    File mImageFile;
    Button mButtonCategory;
    Button mButtonUpdateCategory;
    ImageProvider mImageProvider;
    TextInputEditText mTextInputTitle;
    CircleImageView mCircleImageBack;
    CategoryProvider mCategoryProvider;
    String mTitle;
    String idCategory;
    String image;
    Boolean statusBtnMain;
    AuthProvider mAutProvider;
    AlertDialog mAlertDialog;
    AlertDialog.Builder mBuilderSelector;
    CharSequence options[];

    // photo 1
    String mAbsolutePhotoPath;
    String mPhotoPath;
    File mPhotoFile;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_mantenedor_category);
        mImageViewCategory = findViewById(R.id.imageViewCategory1);
        mImageProvider = new ImageProvider();
        mCategoryProvider = new CategoryProvider();
        mAutProvider = new AuthProvider();
        mTextInputTitle = findViewById(R.id.txtTitleCategory);
        mButtonCategory = findViewById(R.id.btnCategory);
        mButtonUpdateCategory = findViewById(R.id.btnUpdateCategory);
        mCircleImageBack = findViewById(R.id.circleImageBackCategory);

        Intent intent = getIntent();
        idCategory = intent.getStringExtra("idCategory");
        statusBtnMain = intent.getBooleanExtra("status",true);
        image = intent.getStringExtra("image");
        statusButton(statusBtnMain);

        mCircleImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mAlertDialog = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Espere un momento . . .")
                .setCancelable(false)
                .build();
        mBuilderSelector = new AlertDialog.Builder(this);
        mBuilderSelector.setTitle("Selecciona una opción");
        options = new  CharSequence[] {"Imagen de galeria" , "Tomar Foto"};

        mImageViewCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectOptionImage(1);
            }
        });

        mButtonCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickCategory();
            }
        });

        mButtonUpdateCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateCategory();
            }
        });
    }//end on Create

    private void statusButton(Boolean statusButtonMaintainer) {
        if(!statusButtonMaintainer){
            this.mButtonCategory.setEnabled(false);
            this.mButtonCategory.setVisibility(View.INVISIBLE);
            this.mButtonUpdateCategory.setEnabled(true);
            this.mButtonUpdateCategory.setVisibility(View.VISIBLE);
            this.mTextInputTitle.setText(getIntent().getStringExtra("title"));
            Picasso.with(MantenedorCategoryActivity.this).load(image).into(this.mImageViewCategory);
        }
    }//end statusButton

    private void clickCategory() {
        mTitle = mTextInputTitle.getText().toString();
        if(!mTitle.isEmpty()){
            if(mImageFile!=null){
                saveImage(mImageFile);
            }
            else if (mPhotoFile != null ) {
                saveImage(mPhotoFile);
            }
            else{
                Toast.makeText(this, "Debes seleccionar una imagen", Toast.LENGTH_SHORT).show();
            }
        }else{
            mTextInputTitle.setError("Campo obligatorio");
            mTextInputTitle.requestFocus();
        }
    }

    private void saveImage(File imageFile1) {
        mAlertDialog.show();
        mImageProvider.save(MantenedorCategoryActivity.this,imageFile1).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()){
                    mImageProvider.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            final String url = uri.toString();
                            Category category = new Category();
                            category.setImage(url);
                            category.setId(UUID.randomUUID().toString());
                            category.setTitle(mTitle);
                            category.setIdUser(mAutProvider.getUid());
                            mCategoryProvider.save(category).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> taskSave) {
                                    mAlertDialog.dismiss();
                                    if (taskSave.isSuccessful()){
                                        //clearForm();
                                        finish();
                                        Toast.makeText(MantenedorCategoryActivity.this, "Se almacenó correctamente", Toast.LENGTH_SHORT).show();
                                    }else{
                                        Toast.makeText(MantenedorCategoryActivity.this, "No se pudo almacenar la información", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                    });
                }else{
                    mAlertDialog.dismiss();
                    Toast.makeText(MantenedorCategoryActivity.this, "Error al almacenar la imagen", Toast.LENGTH_LONG).show();
                }
            }
        });
    }//end saveImage

    private void updateCategory() {
        mTitle = mTextInputTitle.getText().toString();
        if(!mTitle.isEmpty()){
            if(mImageFile!=null){
                updateImage2(mImageFile);
            }
            else if (mPhotoFile != null ) {
                updateImage2(mPhotoFile);
            }
            else{
                updateImage1();
            }
        }else{
            mTextInputTitle.setError("Campo obligatorio");
            mTextInputTitle.requestFocus();
        }
    }//end updateCategory

    private void updateImage1(){
        mAlertDialog.show();
        Category category = new Category();
        category.setId(idCategory);
        category.setImage(image);
        category.setTitle(mTitle);
        category.setIdUser(mAutProvider.getUid());
        mCategoryProvider.update(category).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> taskSave) {
                mAlertDialog.dismiss();
                if (taskSave.isSuccessful()){
                    finish();
                    Toast.makeText(MantenedorCategoryActivity.this, "Se actualizó correctamente", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(MantenedorCategoryActivity.this, "No se pudo actualizar la información", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }//end updateImage1

    private void updateImage2(File imageFile1) {
        mAlertDialog.show();
        mImageProvider.save(MantenedorCategoryActivity.this,imageFile1).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()){
                    mImageProvider.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            final String url = uri.toString();
                            Category category = new Category();
                            category.setId(idCategory);
                            category.setImage(url);
                            category.setTitle(mTitle);
                            category.setIdUser(mAutProvider.getUid());
                            mCategoryProvider.update(category).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> taskSave) {
                                    mAlertDialog.dismiss();
                                    if (taskSave.isSuccessful()){
                                        finish();
                                        Toast.makeText(MantenedorCategoryActivity.this, "Se actualizó correctamente", Toast.LENGTH_SHORT).show();
                                    }else{
                                        Toast.makeText(MantenedorCategoryActivity.this, "No se pudo actualizar la información", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                    });
                }else{
                    mAlertDialog.dismiss();
                    Toast.makeText(MantenedorCategoryActivity.this, "Error al actualizar la imagen", Toast.LENGTH_LONG).show();
                }
            }
        });
    }//end updateImage2

    private void clearForm() {
        mTextInputTitle.setText("");
        mImageViewCategory.setImageResource(R.drawable.ic_camera_24);
        mTitle =  "";
        mImageFile = null;
    }

    private void selectOptionImage(final int numberImage) {
        mBuilderSelector.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == 0) {
                    if (numberImage == 1) {
                        openGalery(GALLERY_REQUEST_CODE);
                    }
                }
                else if (i == 1){
                    if (numberImage == 1) {
                        takePhoto(PHOTO_REQUEST_CODE);
                    }
                }
            }
        });
        mBuilderSelector.show();
    }

    private void openGalery(int requestCode) {
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent,requestCode);
    }

    private void takePhoto(int requestCode) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createPhotoFile(requestCode);
            } catch(Exception e) {
                Toast.makeText(this, "Hubo un error con el archivo " + e.getMessage(), Toast.LENGTH_LONG).show();
            }

            if (photoFile != null) {
                Uri photoUri = FileProvider.getUriForFile(MantenedorCategoryActivity.this, "com.terrazas.terrazasmallap", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(takePictureIntent, requestCode);
            }
        } Toast.makeText(this, "Seleccionó tomo foto", Toast.LENGTH_SHORT).show();
    }//end takePhoto

    private File createPhotoFile(int requestCode) throws IOException {
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File photoFile = File.createTempFile(
                new Date() + "_photo",
                ".jpg",
                storageDir
        );
        if (requestCode == PHOTO_REQUEST_CODE) {
            mPhotoPath = "file:" + photoFile.getAbsolutePath();
            mAbsolutePhotoPath = photoFile.getAbsolutePath();
        }

        return photoFile;
    }//end createPhotoFile

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK){
            try {
                mPhotoFile = null;
                mImageFile = FileUtil.from(this,data.getData());
                mImageViewCategory.setImageBitmap(BitmapFactory.decodeFile(mImageFile.getAbsolutePath()));
            }catch (Exception e){
                Log.d("Error", "se produjó un error" + e.getMessage());
                Toast.makeText(this, "Se produjó un error" + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }

        /**
         * SELECCION DE FOTOGRAFIA
         */
        if (requestCode == PHOTO_REQUEST_CODE && resultCode == RESULT_OK) {
            mImageFile = null;
            mPhotoFile = new File(mAbsolutePhotoPath);
            Picasso.with(MantenedorCategoryActivity.this).load(mPhotoPath).into(mImageViewCategory);
        }
    }//end onActivityResult
}
