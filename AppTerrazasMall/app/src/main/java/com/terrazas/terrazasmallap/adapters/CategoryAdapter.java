package com.terrazas.terrazasmallap.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.squareup.picasso.Picasso;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.activities.MantenedorCategoryActivity;
import com.terrazas.terrazasmallap.fragments.StandFragment;
import com.terrazas.terrazasmallap.models.Category;
import com.terrazas.terrazasmallap.providers.AuthProvider;
import com.terrazas.terrazasmallap.providers.CategoryProvider;

public class CategoryAdapter extends FirestoreRecyclerAdapter<Category, CategoryAdapter.ViewHolder> {

    Context context;
    CategoryProvider categoryProvider;
    AuthProvider mAuthProvider;
    AlertDialog.Builder alertDialog;

    public CategoryAdapter(@NonNull FirestoreRecyclerOptions<Category> options, Context context) {
        super(options);
        this.context = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Category category) {
        holder.textViewtitle.setText(category.getTitle().toUpperCase());
        if(category.getImage() != null){
            if(!category.getImage().isEmpty()){
                Picasso.with(context).load(category.getImage()).into(holder.imageViewCategory);
            }
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppCompatActivity appCompatActivity = (AppCompatActivity)view.getContext();
                appCompatActivity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container,new StandFragment(category.getId(),category.getTitle()))
                        .addToBackStack(null)
                        .commit();
            }
        });

        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(),MantenedorCategoryActivity.class);
                intent.putExtra("status",false);
                intent.putExtra("idCategory",category.getId());
                intent.putExtra("title",category.getTitle());
                intent.putExtra("image",category.getImage());
                view.getContext().startActivity(intent);
            }
        });

        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog = new AlertDialog.Builder(view.getContext());
                alertDialog.setMessage("¿Realmente desea eliminar la Categoría "+category.getTitle()+"?")
                        .setCancelable(true)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                categoryProvider.delete(category.getId())
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if(task.isSuccessful()){
                                                    Toast.makeText(view.getContext(),"Categoría eliminado correctamente",Toast.LENGTH_SHORT).show();
                                                }else{
                                                    Toast.makeText(view.getContext(),"Error al eliminar Categoría",Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                        .create()
                .show();
            }//end onClick
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_category,parent,false);
        return new CategoryAdapter.ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView textViewtitle;
        ImageView imageViewCategory;
        CardView cardView;
        ImageView ivEdit;
        ImageView ivDelete;
        public ViewHolder(View view){
            super(view);
            textViewtitle = view.findViewById(R.id.txtTitleCVC);
            imageViewCategory= view.findViewById(R.id.ivImageCVC);
            cardView = view.findViewById(R.id.cardViewC);
            ivEdit = view.findViewById(R.id.ibEditCategory);
            ivDelete = view.findViewById(R.id.ibDeleteCategory);

            categoryProvider = new CategoryProvider();
            mAuthProvider = new AuthProvider();

            validarCorreo();
        }
        public void validarCorreo(){
            if(!mAuthProvider.getEmail().equals("terrazashco@hotmail.com")){
                //update
                ivEdit.setEnabled(false);
                ivEdit.setVisibility(View.INVISIBLE);
                //delete
                ivDelete.setEnabled(false);
                ivDelete.setVisibility(View.INVISIBLE);
            }
        }
    }
}
