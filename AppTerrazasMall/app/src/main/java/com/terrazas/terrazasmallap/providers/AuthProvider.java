package com.terrazas.terrazasmallap.providers;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

public class AuthProvider {
    private FirebaseAuth mAuth;

    public AuthProvider(){
        mAuth = FirebaseAuth.getInstance();

    }
    public Task<AuthResult> register (String email, String password){
        return mAuth.createUserWithEmailAndPassword(email,password);
    }
    public Task<AuthResult> login(String email, String password){
        return  mAuth.signInWithEmailAndPassword(email,password);
    }
    public  Task<AuthResult> googleLogin (GoogleSignInAccount googleSignInAccount){
        AuthCredential credential = GoogleAuthProvider.getCredential(googleSignInAccount.getIdToken(),null);
        return  mAuth.signInWithCredential(credential);
    }
    public Task<Void> updatePass(String newpass){
       return mAuth.getCurrentUser().updatePassword(newpass);
    }

    public Task<Void> reauthenticate(String email,String pass){
        AuthCredential credential = EmailAuthProvider
                .getCredential(email,pass);
        return mAuth.getCurrentUser().reauthenticate(credential);
    }

    public Task<Void> sendEmailToPasswordReset(String email){
        return mAuth.sendPasswordResetEmail(email);
    }

    public String getEmail(){
        if(mAuth.getCurrentUser() != null){
            return mAuth.getCurrentUser().getEmail();
        }else{
            return null;
        }
    }
    public  String getUid(){
        if (mAuth.getCurrentUser() != null){
            return  mAuth.getCurrentUser().getUid();
        }else{
            return null;
        }

    }
    public Boolean getIsEmailVerificate(){
        if (mAuth.getCurrentUser() != null){
            return  mAuth.getCurrentUser().isEmailVerified();
        }else{
            return null;
        }
    }

    public FirebaseUser getUserSession() {
        if (mAuth.getCurrentUser() != null) {
            return mAuth.getCurrentUser();
        }else {
            return null;
        }
    }
}
