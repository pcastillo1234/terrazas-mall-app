package com.terrazas.terrazasmallap.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.Query;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.activities.MantenedorCampoActivity;
import com.terrazas.terrazasmallap.adapters.FieldAdapter;
import com.terrazas.terrazasmallap.models.Field;
import com.terrazas.terrazasmallap.providers.AuthProvider;
import com.terrazas.terrazasmallap.providers.FieldProvider;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CampoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CampoFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    View mView;
    FloatingActionButton btnMaintainerField;
    RecyclerView mRecyclerView;
    FieldProvider mFieldProvider;
    AuthProvider mAuthProvider;
    FieldAdapter mFieldAdapter;

    public CampoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CampoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CampoFragment newInstance(String param1, String param2) {
        CampoFragment fragment = new CampoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_campo, container, false);
        btnMaintainerField = mView.findViewById(R.id.fabCD);
        mRecyclerView = mView.findViewById(R.id.recyclerViewFields);
        mFieldProvider = new FieldProvider();
        mAuthProvider = new AuthProvider();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(linearLayoutManager);

        validarCorreo();

        btnMaintainerField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToField();
            }
        });

        return mView;
    }

    public void validarCorreo(){
        if(!mAuthProvider.getEmail().equals("terrazashco@hotmail.com")){
            btnMaintainerField.setEnabled(false);
            btnMaintainerField.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Query query = mFieldProvider.getAll();
        FirestoreRecyclerOptions<Field> options =
                new FirestoreRecyclerOptions.Builder<Field>()
                        .setQuery(query,Field.class)
                        .build();

        mFieldAdapter = new FieldAdapter(options,getContext());
        mRecyclerView.setAdapter(mFieldAdapter);
        mFieldAdapter.startListening();
        //back press
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.container, new CampoFragment());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this,callback);
    }

    @Override
    public void onStop() {
        super.onStop();
        mFieldAdapter.stopListening();
    }

    private void goToField() {
        Intent intent = new Intent(getContext(), MantenedorCampoActivity.class);
        intent.putExtra("status",true);
        startActivity(intent);
    }
}