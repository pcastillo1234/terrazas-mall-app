package com.terrazas.terrazasmallap.models;

public class Stand {

    private String id;
    private String idCategory;
    private String title;
    private String description;
    private String numberStand;
    private String image;
    private String idUser;

    public Stand() {

    }// needed for firebase

    public Stand(String id, String idCategory, String title, String description, String numberStand, String image, String idUser) {
        this.id = id;
        this.idCategory = idCategory;
        this.title = title;
        this.description = description;
        this.numberStand = numberStand;
        this.image = image;
        this.idUser = idUser;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(String idCategory) {
        this.idCategory = idCategory;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNumberStand() {
        return numberStand;
    }

    public void setNumberStand(String numberStand) {
        this.numberStand = numberStand;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }
}
