package com.terrazas.terrazasmallap.fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.models.SuggestionBook;
import com.terrazas.terrazasmallap.providers.AuthProvider;
import com.terrazas.terrazasmallap.providers.SuggestionBookProvider;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

import dmax.dialog.SpotsDialog;

public class DialogSuggestionBook extends DialogFragment implements View.OnClickListener {
    Button btnOk;
    ImageButton ivClose;
    EditText edtIssue;
    EditText edtMessage;
    AuthProvider mAuthProvider;
    SuggestionBookProvider mSuggestionBookProvider;

    String mIssue;
    String mMessage;
    String mDate;
    Date date;

    AlertDialog mAlertDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_suggestion_book,container,false);
        btnOk = view.findViewById(R.id.btnOk);
        edtIssue = view.findViewById(R.id.edtIssue);
        edtMessage = view.findViewById(R.id.edtMessageBS);
        ivClose = view.findViewById(R.id.ivCloseBS);
        mAuthProvider = new AuthProvider();
        mSuggestionBookProvider = new SuggestionBookProvider();
        date = new Date();

        mAlertDialog = new SpotsDialog.Builder()
                .setContext(getContext())
                .setMessage("Espere un momento . . .")
                .setCancelable(false)
                .build();

        btnOk.setOnClickListener(this);
        ivClose.setOnClickListener(this);

        return view ;
    }//end onCreateView

    @Override
    public void onClick(View view) {
        if(view == btnOk){
            saveSB();
        }
        else if(view == ivClose){
            dismiss();
        }
    }// end onClick

    private void saveSB() {
        mIssue = edtIssue.getText().toString();
        mMessage = edtMessage.getText().toString();
        mDate = new SimpleDateFormat("dd/MM/yyyy").format(this.date);
        if(!mIssue.isEmpty() && !mMessage.isEmpty()){
            mAlertDialog.show();
            SuggestionBook suggestionBook = new SuggestionBook();
            suggestionBook.setId(UUID.randomUUID().toString());
            suggestionBook.setIssue(mIssue);
            suggestionBook.setMessage(mMessage);
            suggestionBook.setDate(mDate);
            suggestionBook.setIdUser(mAuthProvider.getUid());
            mSuggestionBookProvider.save(suggestionBook).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    mAlertDialog.dismiss();
                    if(task.isSuccessful()){
                        dismiss();
                        Toast.makeText(getContext(),"Sugerencia enviada correctamente",Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getContext(),"Error al enviar",Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }else{
            Toast.makeText(getContext(),"Complete los cuadros de texto",Toast.LENGTH_SHORT).show();
        }
    }
}
