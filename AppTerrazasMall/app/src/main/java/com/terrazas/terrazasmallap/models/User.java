package com.terrazas.terrazasmallap.models;

public class User {
    private  String id;
    private String email;
    private  String username;
    private  String dni;
    private String cellphone;


    public User(){

    }

    public User(String id, String email, String username ,String dni, String cellphone) {
        this.id = id;
        this.email = email;
        this.username = username;
        this.dni = dni;
        this.cellphone = cellphone;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }
}
