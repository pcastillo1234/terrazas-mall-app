package com.terrazas.terrazasmallap.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.models.FieldReservation;
import com.terrazas.terrazasmallap.models.Schedule;
import com.terrazas.terrazasmallap.providers.AuthProvider;
import com.terrazas.terrazasmallap.providers.FieldReservationProvider;

import java.util.List;
import java.util.UUID;

import dmax.dialog.SpotsDialog;

public class ConfirmFieldReservation extends Fragment {

    AuthProvider mAuthProvider;
    FieldReservationProvider mFieldReservationProvider;

    TextView mtxtName;
    TextView mtxtDni;
    TextView mtxtTitleField;
    TextView mtxtdate;
    TextView mtxtSchedule;
    TextView mtxtAmount;

    String name;
    String dni;
    String cellphone;
    String idField;
    String titleField;
    String date;
    String price;
    String timeInterval;
    String startTime;
    String endTime;
    String idSchedule;
    String descriptionSchedule;

    ImageButton ivReturn;
    TextView txtCancel;
    Button btnConfirm;

    AlertDialog.Builder alertDialog;
    AlertDialog mAlertDialog;

    String[] countSelectedSchedule;
    int totalAmount;
    List<Schedule> mScheduleList;

    public ConfirmFieldReservation() {
    }

    public ConfirmFieldReservation(String name, String dni, String cellphone, String idField, String titleField, String date, String price,String timeInterval,String startTime,String endTime,String idSchedule,List<Schedule> mScheduleList) {
        this.name = name;
        this.dni = dni;
        this.cellphone = cellphone;
        this.idField = idField;
        this.titleField = titleField;
        this.date = date;
        this.price = price;
        this.timeInterval = timeInterval;
        this.startTime = startTime;
        this.endTime = endTime;
        this.idSchedule = idSchedule;
        this.mScheduleList = mScheduleList;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sport_field_confirm_reservation,container,false);
        mtxtName = view.findViewById(R.id.txtNameCSFR);
        mtxtDni = view.findViewById(R.id.txtDniCSFR);
        mtxtTitleField = view.findViewById(R.id.txtFieldCSFR);
        mtxtdate = view.findViewById(R.id.txtDateCSFR);
        mtxtSchedule = view.findViewById(R.id.txtScheduleCSFR);
        mtxtAmount = view.findViewById(R.id.txtAmountCSFR);
        mAuthProvider = new AuthProvider();
        mFieldReservationProvider = new FieldReservationProvider();
        ivReturn = view.findViewById(R.id.ivBackFR);
        txtCancel = view.findViewById(R.id.lblCancelCSFR);
        btnConfirm = view.findViewById(R.id.btnConfirmCSFR);

        waitingDialog();

        ivReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToReservation();
            }
        });

        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancelReservation();
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalizeReservation();
            }
        });

        return view;
    }

    private void waitingDialog() {
        mAlertDialog = new SpotsDialog.Builder()
                .setContext(getContext())
                .setMessage("Espere un momento . . .")
                .setCancelable(false)
                .build();
    }

    private void finalizeReservation() {
        mAlertDialog.show();
        FieldReservation fieldReservation = new FieldReservation();
        fieldReservation.setId(UUID.randomUUID().toString());
        fieldReservation.setIdUser(mAuthProvider.getUid());
        fieldReservation.setName(name);
        fieldReservation.setDni(dni);
        fieldReservation.setCellphone(cellphone);
        fieldReservation.setIdField(idField);
        fieldReservation.setTitleField(titleField);
        fieldReservation.setDate(date);
        fieldReservation.setAmount(String.valueOf(totalAmount));
        fieldReservation.setIdSchedule(idSchedule);
        fieldReservation.setDescriptionSchedule(descriptionSchedule);
        mFieldReservationProvider.save(fieldReservation).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                mAlertDialog.dismiss();
                  if(task.isSuccessful()){
                      goToFieldFragment();
                      Toast.makeText(getContext(),"Reserva realizada correctamente",Toast.LENGTH_SHORT).show();
                  }else{
                      Toast.makeText(getContext(),"Error al realizar la reserva... Intente nuevamente",Toast.LENGTH_SHORT).show();
                  }
            }
        });
    }

    private void goToFieldFragment() {
        AppCompatActivity activity = (AppCompatActivity)getContext();
        activity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.container,new CampoFragment())
                .addToBackStack(null)
                .commit();
    }

    private void cancelReservation() {
        alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setMessage("Confirmar cancelación de reserva")
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        goToFieldFragment();
                        Toast.makeText(getContext(),"Reserva cancelada.",Toast.LENGTH_SHORT).show();
                    }
                }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        })
                .create()
                .show();
    }

    @Override
    public void onStart() {
        super.onStart();
        //
        countSelectedSchedule = idSchedule.split(",");
        //
        totalAmount = Integer.parseInt(price) * countSelectedSchedule.length;
        //
        String newSchedule = "";
        for(Schedule scheduleClass : mScheduleList){
            for(int i=0;i<countSelectedSchedule.length;i++){
                if(countSelectedSchedule[i].equals(scheduleClass.getIdSchedule())){
                    if(newSchedule.isEmpty()){
                        newSchedule +=scheduleClass.getTime()+" "+getAM_PM(scheduleClass.getTime())+" - "+nextTime(scheduleClass.getTime())+" "+getAM_PM(nextTime(scheduleClass.getTime()));
                    }else{
                        newSchedule +="\n"+scheduleClass.getTime()+" "+getAM_PM(scheduleClass.getTime())+" - "+nextTime(scheduleClass.getTime())+" "+getAM_PM(nextTime(scheduleClass.getTime()));
                    }
                }
            }
        }
        descriptionSchedule = newSchedule;
        Log.d("Message SC",descriptionSchedule);
        //
        mtxtName.setText(name);
        mtxtDni.setText(dni);
        mtxtTitleField.setText(titleField);
        mtxtdate.setText(date);
        mtxtSchedule.setText(descriptionSchedule);
        mtxtAmount.setText("S/"+String.valueOf(totalAmount));
    }

    private void goToReservation() {
        AppCompatActivity activity = (AppCompatActivity)getContext();
        activity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.container,new SportFieldReservationFragment(idField,titleField,price,timeInterval,startTime,endTime))
                .addToBackStack(null)
                .commit();
    }//end goToReservation

    public String nextTime(String time){
        //afterTime
        String[] a = time.split(":");
        int a0 = Integer.parseInt(a[0]) * 3600;
        int a1 = Integer.parseInt(a[1]) * 60;
        int a2 = a0+a1;
        //interval
        int b = Integer.parseInt(timeInterval) * 60;
        //newHour
        int c = 0;
        //newMinute
        int d = 0;
        //newTime
        String newTime = "";
        a2 += b;
        c = a2/3600;
        d = (a2%3600)/60;
        if(c<10){
            newTime = "0"+String.valueOf(c)+":";
        }else{
            newTime = String.valueOf(c)+":";
        }
        if(d<10){
            newTime += "0"+String.valueOf(d);
        }else{
            newTime += String.valueOf(d);
        }
        return newTime;
    }

    private String getAM_PM(@NonNull String time) {
        String[] a = time.split(":");
        int a0 = Integer.parseInt(a[0]);
        String AM_PM;
        if(a0 < 12) {
            AM_PM = "a.m.";
        } else {
            AM_PM = "p.m.";
        }
        return AM_PM;
    }
}
