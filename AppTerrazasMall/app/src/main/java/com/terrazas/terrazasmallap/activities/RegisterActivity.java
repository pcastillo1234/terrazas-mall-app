package com.terrazas.terrazasmallap.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.models.User;
import com.terrazas.terrazasmallap.providers.AuthProvider;
import com.terrazas.terrazasmallap.providers.UserProvider;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;

public class RegisterActivity extends AppCompatActivity {
    Boolean iv = false;
    Boolean iv_confirm = false;
    CircleImageView mcircleImageBack;
    TextInputEditText mTextInputNombre;
    TextInputEditText mTextInputEmail;
    TextInputEditText mTextInputPassword;
    TextInputEditText mTextInputConfirmPassword;
    ImageView ivVisibility_pass;
    ImageView ivVisibility_pass_confirm;
    Button mButtonRegister;
    AuthProvider mAuthProvider;
    UserProvider mUserProvider;
    AlertDialog mDialog;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mcircleImageBack= findViewById(R.id.circleImageBack);
        mTextInputNombre = findViewById(R.id.txtNombre);
        mTextInputEmail = findViewById(R.id.txtEmail);
        mTextInputPassword = findViewById(R.id.txtPassword);
        mTextInputConfirmPassword = findViewById(R.id.txtConfirmPassword);
        mButtonRegister = findViewById(R.id.btnRegistro);
        ivVisibility_pass = findViewById(R.id.ivVisibility_pass);
        ivVisibility_pass_confirm = findViewById(R.id.ivVisibility_pass_confirm);
        mAuthProvider = new AuthProvider();
        mUserProvider = new UserProvider();
        mDialog = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Espere un momento . . .")
                .setCancelable(false)
                .build();

        mcircleImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mButtonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();

            }
        });

        ivVisibility_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!iv){
                    mTextInputPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    mTextInputPassword.setSelection(mTextInputPassword.getText().length());
                    ivVisibility_pass.setBackground(RegisterActivity.this.getDrawable(R.drawable.ic_baseline_visibility_24));
                    iv = true;
                }else{
                    mTextInputPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    mTextInputPassword.setSelection(mTextInputPassword.getText().length());
                    ivVisibility_pass.setBackground(RegisterActivity.this.getDrawable(R.drawable.ic_baseline_visibility_off_24));
                    iv = false;
                }
                Log.d("Estado","Boolean:"+iv);
            }
        });

        ivVisibility_pass_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!iv_confirm){
                    mTextInputConfirmPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    mTextInputConfirmPassword.setSelection(mTextInputConfirmPassword.getText().length());
                    ivVisibility_pass_confirm.setBackground(RegisterActivity.this.getDrawable(R.drawable.ic_baseline_visibility_24));
                    iv_confirm = true;
                }else{
                    mTextInputConfirmPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    mTextInputConfirmPassword.setSelection(mTextInputConfirmPassword.getText().length());
                    ivVisibility_pass_confirm.setBackground(RegisterActivity.this.getDrawable(R.drawable.ic_baseline_visibility_off_24));
                    iv_confirm = false;
                }
                Log.d("Estado","Boolean:"+iv_confirm);
            }
        });
    }



    private void register() {
        String username = mTextInputNombre.getText().toString();
        String email = mTextInputEmail.getText().toString();
        String password = mTextInputPassword.getText().toString();
        String confirmPassword = mTextInputConfirmPassword.getText().toString();

        if(!username.isEmpty() && !email.isEmpty() && !password.isEmpty() && !confirmPassword.isEmpty()){
            if(isEmailValid(email)){
                if (password.equals(confirmPassword)){
                     if(password.length()>=6) {
                         creteUser(username, email, password);
                     } else{
                         Toast.makeText(this, "La contraseñ debe de tener mínimo 6 caracteres", Toast.LENGTH_SHORT).show();
                     }
                } else{
                    Toast.makeText(this, "Las contraseñas no coinciden", Toast.LENGTH_SHORT).show();
                }
            } else{
                Toast.makeText(this, "El email no es válido", Toast.LENGTH_SHORT).show();
            }
        } else{
            Toast.makeText(this, "Complete todos los campos", Toast.LENGTH_SHORT).show();
        }

    }

    private void creteUser(final String username,final String email, String password) {
        mDialog.show();
        mAuthProvider.register(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){

                    String id = mAuthProvider.getUid() ;

                    User user = new User();
                    user.setId(id);
                    user.setEmail(email);
                    user.setUsername(username);
                    user.setDni("");
                    user.setCellphone("");
                    mUserProvider.create(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            mDialog.dismiss();
                            if(task.isSuccessful()){
                                Intent intent = new Intent(RegisterActivity.this,MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                user.sendEmailVerification();
                                startActivity(intent);

                             }else{
                                Toast.makeText(RegisterActivity.this, "No se pudo alamcenar el usuario", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }else{
                    mDialog.dismiss();
                    Toast.makeText(RegisterActivity.this, "No se pudo registrar al usuario", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}