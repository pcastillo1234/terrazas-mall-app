package com.terrazas.terrazasmallap.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.squareup.picasso.Picasso;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.activities.MantenedorCampoActivity;
import com.terrazas.terrazasmallap.activities.MantenedorCategoryActivity;
import com.terrazas.terrazasmallap.fragments.FieldDetailFragment;
import com.terrazas.terrazasmallap.models.Field;
import com.terrazas.terrazasmallap.providers.AuthProvider;
import com.terrazas.terrazasmallap.providers.FieldProvider;

public class FieldAdapter extends FirestoreRecyclerAdapter<Field,FieldAdapter.ViewHolder> {

    Context context;
    FieldProvider fieldProvider;
    AuthProvider mAuthProvider;
    AlertDialog.Builder alertDialog;
    public FieldAdapter(@NonNull FirestoreRecyclerOptions<Field> options, Context context) {
        super(options);
        this.context = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Field field) {
        holder.textViewtitle.setText(field.getTitle());
        holder.getTextViewDirection.setText(field.getDirection());
        if(field.getImage() != null){
            if(!field.getImage().isEmpty()){
                Picasso.with(context).load(field.getImage()).into(holder.imageViewField);
            }
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppCompatActivity activity = (AppCompatActivity)view.getContext();
                activity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container,new FieldDetailFragment(field.getId(),field.getImage(),field.getDirection(), field.getDescription(),
                                field.getTitle(),field.getPrice(),field.getTimeInterval(),field.getStartTime(),field.getEndTime(),field.getDays()))
                        .addToBackStack(null)
                        .commit();
            }
        });

        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), MantenedorCampoActivity.class);
                intent.putExtra("status",false);
                intent.putExtra("idField",field.getId());
                intent.putExtra("title",field.getTitle());
                intent.putExtra("image",field.getImage());
                intent.putExtra("description",field.getDescription());
                intent.putExtra("direction",field.getDirection());
                intent.putExtra("price",field.getPrice());
                intent.putExtra("timeInterval",field.getTimeInterval());
                intent.putExtra("startTime",field.getStartTime());
                intent.putExtra("endTime",field.getEndTime());
                intent.putExtra("days",field.getDays());
                view.getContext().startActivity(intent);
            }
        });

        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog = new AlertDialog.Builder(view.getContext());
                alertDialog.setMessage("¿Realmente desea eliminar "+field.getTitle()+"?")
                        .setCancelable(true)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                fieldProvider.delete(field.getId())
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if(task.isSuccessful()){
                                                    Toast.makeText(view.getContext(),"Campo deportivo eliminado correctamente",Toast.LENGTH_SHORT).show();
                                                }else{
                                                    Toast.makeText(view.getContext(),"Error al eliminar campo deportivo",Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                        .create()
                        .show();
            }//end onClick
        });

        holder.ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String message = "";
                message +=field.getTitle();
                message +="\n"+"Descripción: "+field.getDescription();
                message +="\n"+"Ubícanos en "+field.getDirection();
                message +="\n"+"Descarga nuestra aplicación y conoce más sobre lo que brindamos:";
                message +="\n"+"link";
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT, message);
                view.getContext().startActivity(Intent.createChooser(share, "Compartir campo deportivo mediante:"));
            }//end onClick
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_field,parent,false);
        return new FieldAdapter.ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView textViewtitle;
        TextView getTextViewDirection;
        ImageView imageViewField;
        CardView cardView;
        ImageView ivEdit;
        ImageView ivDelete;
        ImageView ivShare;
        public ViewHolder(View view){
            super(view);
            textViewtitle = view.findViewById(R.id.txtTitleCVField);
            getTextViewDirection = view.findViewById(R.id.txtDirectionCVField);
            imageViewField= view.findViewById(R.id.ivImageCVField);
            cardView = view.findViewById(R.id.cardViewField);
            ivEdit = view.findViewById(R.id.ibEditField);
            ivDelete = view.findViewById(R.id.ibDeleteField);
            ivShare = view.findViewById(R.id.ibShareField);

            fieldProvider = new FieldProvider();
            mAuthProvider = new AuthProvider();

            validarCorreo();
        }
        public void validarCorreo(){
            if(!mAuthProvider.getEmail().equals("terrazashco@hotmail.com")){
                //update
                ivEdit.setEnabled(false);
                ivEdit.setVisibility(View.INVISIBLE);
                //delete
                ivDelete.setEnabled(false);
                ivDelete.setVisibility(View.INVISIBLE);
            }
        }
    }//end ViewHolder
}
