package com.terrazas.terrazasmallap.models;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.util.List;
import java.util.Locale;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.NumberFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

public class ExcelExportReservation {
    public static final String CURRENCY_PERU = "[$S/-409]";

    public static void export(List<FieldReservation> mList){
        File fileSD = Environment.getExternalStorageDirectory();
        String fileName = "Libro de Reservaciones.xls";
        File directory = new File(fileSD.getAbsolutePath());

        if(!directory.isDirectory()){
            directory.mkdirs();
        }
        try{
            File file = new File(directory,fileName);
            WorkbookSettings workbookSettings = new WorkbookSettings();
            workbookSettings.setLocale(new Locale(Locale.ENGLISH.getLanguage(),Locale.ENGLISH.getCountry()));
            WritableWorkbook workbook;
            workbook = Workbook.createWorkbook(file,workbookSettings);

            //Header
            WritableFont writableFontHeader = new WritableFont(WritableFont.createFont("Arial"), WritableFont.DEFAULT_POINT_SIZE, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.WHITE);
            WritableCellFormat cellFormatHeader = new WritableCellFormat(writableFontHeader);
            cellFormatHeader.setAlignment(Alignment.CENTRE);
            cellFormatHeader.setVerticalAlignment(VerticalAlignment.CENTRE);
            cellFormatHeader.setBorder(Border.ALL, BorderLineStyle.THICK, Colour.BLACK);
            cellFormatHeader.setBackground(Colour.GRAY_80);
            cellFormatHeader.setWrap(true);

            //Body
            WritableCellFormat cellFormatBody = new WritableCellFormat();
            cellFormatBody.setAlignment(Alignment.CENTRE);
            cellFormatBody.setVerticalAlignment(VerticalAlignment.CENTRE);
            cellFormatBody.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
            cellFormatBody.setWrap(true);

            /*//Body Schedule
            WritableCellFormat cellFormatBodySchedule = new WritableCellFormat();
            cellFormatBodySchedule.setAlignment(Alignment.JUSTIFY);
            cellFormatBodySchedule.setVerticalAlignment(VerticalAlignment.CENTRE);
            cellFormatBodySchedule.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
            cellFormatBodySchedule.setWrap(true);*/


            //Body currency
            NumberFormat currencyFormat = new NumberFormat(CURRENCY_PERU + " ###,###.00", NumberFormat.COMPLEX_FORMAT);
            WritableCellFormat cellFormatBodyCurrency = new WritableCellFormat(currencyFormat);
            cellFormatBodyCurrency.setAlignment(Alignment.CENTRE);
            cellFormatBodyCurrency.setVerticalAlignment(VerticalAlignment.CENTRE);
            cellFormatBodyCurrency.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
            cellFormatBodyCurrency.setWrap(true);

            WritableSheet sheet1 = workbook.createSheet("Libro de Reservaciones",0);
            sheet1.addCell(new Label(0,0,"FECHA",cellFormatHeader));
            sheet1.addCell(new Label(1,0,"NOMBRE",cellFormatHeader));
            sheet1.addCell(new Label(2,0,"DNI",cellFormatHeader));
            sheet1.addCell(new Label(3,0,"CELULAR",cellFormatHeader));
            sheet1.addCell(new Label(4,0,"CAMPO DEPORTIVO",cellFormatHeader));
            sheet1.addCell(new Label(5,0,"HORARIO DE ALQUILER",cellFormatHeader));
            sheet1.addCell(new Label(6,0,"MONTO TOTAL",cellFormatHeader));

            int cont = 1;
            for(FieldReservation fieldReservation : mList){
                sheet1.addCell(new Label(0,cont,fieldReservation.getDate(),cellFormatBody));
                sheet1.addCell(new Label(1,cont,fieldReservation.getName(),cellFormatBody));
                sheet1.addCell(new Label(2,cont,fieldReservation.getDni(),cellFormatBody));
                sheet1.addCell(new Label(3,cont,fieldReservation.getCellphone(),cellFormatBody));
                sheet1.addCell(new Label(4,cont,fieldReservation.getTitleField(),cellFormatBody));
                sheet1.addCell(new Label(5,cont,fieldReservation.getDescriptionSchedule(),cellFormatBody));
                sheet1.addCell(new Number(6,cont,Double.parseDouble(fieldReservation.getAmount()),cellFormatBodyCurrency));
                cont++;
                Log.d("Reservation Data", fieldReservation.getDate()+" | "+fieldReservation.getName()+" | "+fieldReservation.getDni()+" | "+fieldReservation.getCellphone()
                        +" | "+fieldReservation.getTitleField()+" | "+fieldReservation.getAmount());
            }
            sheet1.setColumnView(0,12);
            sheet1.setColumnView(1,30);
            sheet1.setColumnView(2,10);
            sheet1.setColumnView(3,11);
            sheet1.setColumnView(4,40);
            sheet1.setColumnView(5,23);
            sheet1.setColumnView(6,10);
            workbook.write();
            workbook.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }//end export
}
