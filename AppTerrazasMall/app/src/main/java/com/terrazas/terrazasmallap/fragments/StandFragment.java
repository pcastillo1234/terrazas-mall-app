package com.terrazas.terrazasmallap.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.Query;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.activities.MantenedorStandsActivity;
import com.terrazas.terrazasmallap.adapters.StandAdapter;
import com.terrazas.terrazasmallap.models.Stand;
import com.terrazas.terrazasmallap.providers.AuthProvider;
import com.terrazas.terrazasmallap.providers.StandProvider;

/**
 * A simple {@link Fragment} subclass.

 */
public class StandFragment extends Fragment {
    View mView;
    FloatingActionButton mFab;
    RecyclerView mRecyclerView;
    TextView txtTitleCategory;
    StandProvider mStandProvider;
    StandAdapter mStandAdapter;
    AuthProvider mAuthProvider;
    String idCategory;
    String titleCategory;

    public StandFragment() {
        // Required empty public constructor
    }

    public StandFragment(String idcategory) {
        this.idCategory = idcategory;
    }

    public StandFragment(String idcategory,String titleCategory) {
        this.idCategory = idcategory;
        this.titleCategory = titleCategory;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       mView = inflater.inflate(R.layout.fragment_stands, container, false);
       mFab = mView.findViewById(R.id.fabStand);
       mRecyclerView = mView.findViewById(R.id.riclerViewStand);
       txtTitleCategory = mView.findViewById(R.id.lblCategoryStand);
       mStandProvider = new StandProvider();
       mAuthProvider = new AuthProvider();

       categoryTitle();

       LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
       mRecyclerView.setLayoutManager(linearLayoutManager);

       validarCorreo();

       mFab.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               goToStand();
           }
       });
        return mView;
    }

    private void categoryTitle() {
        if(!titleCategory.isEmpty()){
            txtTitleCategory.setText("Categoría: "+ucFirst(titleCategory));
        }
    }

    public static String ucFirst(String str) {
        if (str == null || str.isEmpty()) return str;
        else return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    public void validarCorreo(){
        if(!mAuthProvider.getEmail().equals("terrazashco@hotmail.com")){
            mFab.setEnabled(false);
            mFab.setVisibility(View.INVISIBLE);
        }
    }

    public void onStart() {
        super.onStart();
        Query query = mStandProvider.getAll(idCategory);
        FirestoreRecyclerOptions<Stand> options =
                new FirestoreRecyclerOptions.Builder<Stand>()
                        .setQuery(query, Stand.class)
                        .build();

        mStandAdapter = new StandAdapter(options,getContext());
        mRecyclerView.setAdapter(mStandAdapter);
        mStandAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        mStandAdapter.stopListening();
    }

    private void goToStand() {
        Intent intent = new Intent(getContext(), MantenedorStandsActivity.class);
        intent.putExtra("idCategory",idCategory);
        intent.putExtra("status",true);
        startActivity(intent);
    }
}