package com.terrazas.terrazasmallap.models;

public class Field {

    private String id;
    private String idUser;
    private String title;
    private String direction;
    private String description;
    private String price;
    private String image;
    private String days;
    private String timeInterval;
    private String startTime;
    private String endTime;

    public Field() {
    }//needed for firebase

    public Field(String id, String idUser, String title, String direction, String description, String price, String image, String days,String timeInterval,String startTime,String endTime) {
        this.id = id;
        this.idUser = idUser;
        this.title = title;
        this.direction = direction;
        this.description = description;
        this.price = price;
        this.image = image;
        this.days = days;
        this.timeInterval = timeInterval;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(String timeInterval) {
        this.timeInterval = timeInterval;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
