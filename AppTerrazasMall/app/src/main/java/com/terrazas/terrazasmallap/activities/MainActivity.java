package com.terrazas.terrazasmallap.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.firestore.DocumentSnapshot;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.fragments.DialogPasswordReset;
import com.terrazas.terrazasmallap.models.User;
import com.terrazas.terrazasmallap.providers.AuthProvider;
import com.terrazas.terrazasmallap.providers.UserProvider;

import dmax.dialog.SpotsDialog;


public class MainActivity extends AppCompatActivity {
    Boolean iv = false;
    ImageView ivVisibilityPass;
    TextInputEditText edtEmail;
    TextInputEditText edtPassword;
    TextView tvRegistro;
    TextView tvPasswordReset;
    Button btnLogin;
    AuthProvider mAuthProvider;
    UserProvider mUserProvider;
    SignInButton mButtonGoogle;
    private GoogleSignInClient mGoogleSignInClient;
    private final int REQUEST_CODE_GOOGLE = 1;
    AlertDialog mDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ivVisibilityPass = findViewById(R.id.ivVisibility_pass);
        tvRegistro = findViewById(R.id.TextviewRegister);
        tvPasswordReset = findViewById(R.id.txtPasswordReset);
        edtEmail = findViewById(R.id.textInputEmail);
        edtPassword= findViewById(R.id.textInputPassword);
        btnLogin = findViewById(R.id.btnLogin);
        mButtonGoogle = findViewById(R.id.btnLoginGoogle);
        mAuthProvider =  new AuthProvider();
        mUserProvider= new UserProvider();
        mDialog = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Espere un momento . . .")
                .setCancelable(false)
                .build();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this , gso);
        mGoogleSignInClient.signOut();

        mButtonGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInGoogle();
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        tvRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
        ivVisibilityPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!iv){
                    edtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    edtPassword.setSelection(edtPassword.getText().length());
                    ivVisibilityPass.setBackground(MainActivity.this.getDrawable(R.drawable.ic_baseline_visibility_24));
                    iv = true;
                }else{
                    edtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    edtPassword.setSelection(edtPassword.getText().length());
                    ivVisibilityPass.setBackground(MainActivity.this.getDrawable(R.drawable.ic_baseline_visibility_off_24));
                    iv = false;
                }
                Log.d("Estado","Boolean:"+iv);
            }
        });

        tvPasswordReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogPasswordReset();
            }
        });

    }

    private void showDialogPasswordReset() {
        DialogPasswordReset dialogPasswordReset = new DialogPasswordReset();
        dialogPasswordReset.setCancelable(true);
        dialogPasswordReset.show(getSupportFragmentManager(),"tag");
    }


    private void signInGoogle() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, REQUEST_CODE_GOOGLE);
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_GOOGLE) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w("Error", "Google sign in failed", e);
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        mDialog.show();
        mAuthProvider.googleLogin(acct).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            String id = mAuthProvider.getUid();
                            checkUserExist(id);

                        } else {
                            mDialog.dismiss();
                            Log.w("Error", "signInWithCredential:failure", task.getException());
                            Toast.makeText(MainActivity.this, "No se pudo inciciar sesion", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void checkUserExist(String id) {
        mUserProvider.getUser(id).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
        @Override
        public void onSuccess(DocumentSnapshot documentSnapshot) {
            if(documentSnapshot.exists()){
                mDialog.dismiss();
                Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                startActivity(intent);
            } else{
                String email = mAuthProvider.getEmail();
                User user = new User();
                user.setEmail(email);
                user.setId(id);
                user.setDni("");
                user.setCellphone("");
                mUserProvider.create(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        mDialog.dismiss();
                            if(task.isSuccessful()){
                                Intent intent1 = new Intent(MainActivity.this,CompletarPerfillActivity.class);
                                startActivity(intent1);
                            }
                            else{
                                Toast.makeText(MainActivity.this, "No se pudo almacenar la informcion del usuario", Toast.LENGTH_SHORT).show();
                            }
                    }
                });
            }

        }
    });

    }

    //METODO PARA LOGEARSE
    // DESPUES DE REALIZAR EL REGISTRO
    private void login() {
        String email = edtEmail.getText().toString();
        String password = edtPassword.getText().toString();
        if(!email.isEmpty()){
            if(!password.isEmpty()){
                mDialog.show();
                mAuthProvider.login(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        mDialog.dismiss();
                        if(task.isSuccessful()){
                            if(mAuthProvider.getIsEmailVerificate()){
                                Intent intent = new Intent(MainActivity.this,HomeActivity.class);
                                startActivity(intent);
                            }else{
                                Toast.makeText(MainActivity.this, "Email "+mAuthProvider.getEmail()+" no verificado", Toast.LENGTH_SHORT).show();
                                Log.d("Status EmailVerificate",mAuthProvider.getIsEmailVerificate().toString());
                            }
                        }else {
                            Toast.makeText(MainActivity.this, "Contraseña y email incorrectos", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                Log.d("CAMPO","email: " +email);
                Log.d("CAMPO","password: " +password);
            }else{
                Toast.makeText(MainActivity.this,"Ingrese una contraseña",Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(MainActivity.this,"Ingrese un email",Toast.LENGTH_SHORT).show();
        }
    }//end login

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        edtEmail.setText("");
        edtPassword.setText("");
        finish();
        startActivity(getIntent());
    }
}