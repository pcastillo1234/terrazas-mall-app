package com.terrazas.terrazasmallap.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.adapters.ReservationListAdapter;
import com.terrazas.terrazasmallap.models.ExcelExportReservation;
import com.terrazas.terrazasmallap.models.FieldReservation;
import com.terrazas.terrazasmallap.providers.FieldReservationProvider;

import java.util.ArrayList;
import java.util.List;

import dmax.dialog.SpotsDialog;

public class ListReservationFragment extends Fragment {
    ImageButton ivBack;
    ImageButton ivReservationExportData;
    ListView mAllReservationList;
    FieldReservationProvider mFieldReservationProvider;
    ReservationListAdapter mReservationListAdapter;
    List<FieldReservation> mList;
    AlertDialog.Builder alertDialog;
    AlertDialog mAlertDialog;

    public ListReservationFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_reservation,container,false);
        ivBack = view.findViewById(R.id.ivBack);
        ivReservationExportData = view.findViewById(R.id.ivExportDataR);
        mAllReservationList = view.findViewById(R.id.allReservationList);
        mFieldReservationProvider = new FieldReservationProvider();

        waitingDialog();

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                returnProfile();
            }
        });

        ivReservationExportData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exportExcelR();
            }
        });

        return view;
    }

    private void exportExcelR() {
        alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setMessage("Exportar a Excel las Reservaciones")
                .setCancelable(true)
                .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                         && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                            mAlertDialog.show();
                            ExcelExportReservation.export(mList);
                            mAlertDialog.dismiss();
                            Toast.makeText(getContext(),"Excel generado correctamente en el almacenamiento interno de su dispositivo.",Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getContext(),"Habilite los permisos para generar el documento",Toast.LENGTH_SHORT).show();
                            askForPermission(Manifest.permission.READ_EXTERNAL_STORAGE, 0);
                            askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, 1);
                        }
                    }
                }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        })
                .create()
                .show();
    }//end exportExcel

    private void waitingDialog() {
        mAlertDialog = new SpotsDialog.Builder()
                .setContext(getContext())
                .setMessage("Espere un momento . . .")
                .setCancelable(false)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();
        mFieldReservationProvider.getAll().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                mList = new ArrayList<>();
                if(task.isSuccessful()){
                    for(QueryDocumentSnapshot document : task.getResult()){
                        FieldReservation mData = document.toObject(FieldReservation.class);
                        mList.add(mData);
                    }//end for
                    mReservationListAdapter = new ReservationListAdapter(getContext(),mList);
                    mAllReservationList.setAdapter(mReservationListAdapter);
                }else{
                    Log.d("Error","Error getting documents: ",task.getException());
                }
            }
        });
    }

    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(getActivity(), permission) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, requestCode);
        } else {
            Log.d("Permission", permission + " habilitado.");
        }
    }

    private void returnProfile() {
        AppCompatActivity appCompatActivity = (AppCompatActivity)this.getContext();
        appCompatActivity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.container,new PerfilFragment())
                .addToBackStack(null)
                .commit();
    }
}
