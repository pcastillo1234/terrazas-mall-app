package com.terrazas.terrazasmallap.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.models.Events;
import com.terrazas.terrazasmallap.providers.AuthProvider;
import com.terrazas.terrazasmallap.providers.EventsProvider;
import com.terrazas.terrazasmallap.providers.ImageProvider;
import com.terrazas.terrazasmallap.utils.FileUtil;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;

public class MantenedorEventsActivity extends AppCompatActivity {

    ImageView mImageViewEvents;
    private final int GALLERY_REQUEST_CODE = 1;
    private final int PHOTO_REQUEST_CODE = 2;
    File mImageFile;
    Button mButtonEvents;
    Button mButtonUpdateEvents;
    ImageProvider mImageProvider;
    TextInputEditText mTextInputTitle;
    TextInputEditText mTextInputDescription;
    TextInputEditText mTextInputDirection;
    TextInputEditText mTextInputPrice;
    TextInputEditText mTextInputIntervalTime;
    TextInputEditText mTextInputCellphone;
    TextInputEditText mTextInputWhatsapp;
    CircleImageView mCircleImageBack;
    EventsProvider mEventsProvider;
    String mTitle;
    String mDescription;
    String mDirection;
    String mPrice;
    String mIntervalTime;
    String mCellphone;
    String mWhatsapp;
    String idEvents;
    String image;
    Boolean statusBtnMain;
    AuthProvider mAutProvider;
    AlertDialog mAlertDialog;
    AlertDialog.Builder mBuilderSelector;
    CharSequence options[];

    // photo 1
    String mAbsolutePhotoPath;
    String mPhotoPath;
    File mPhotoFile;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_mantenedor_events);
        mImageViewEvents = findViewById(R.id.imageViewEvents1);
        mImageProvider = new ImageProvider();
        mEventsProvider = new EventsProvider();
        mAutProvider = new AuthProvider();
        mTextInputTitle = findViewById(R.id.txtTitleSE);
        mTextInputDescription = findViewById(R.id.txtDescriptionSE);
        mTextInputDirection =findViewById(R.id.txtDireccionSE);
        mTextInputPrice =findViewById(R.id.txtPrecioAlquilerSE);
        mTextInputIntervalTime =findViewById(R.id.txtIntervalTimeSE);
        mTextInputCellphone =findViewById(R.id.txtCelularSE);
        mTextInputWhatsapp =findViewById(R.id.txtWhatsappSE);
        mButtonEvents = findViewById(R.id.btnEvents);
        mButtonUpdateEvents = findViewById(R.id.btnUpdateEvents);
        mCircleImageBack = findViewById(R.id.circleImageBackSE);

        Intent intent = getIntent();
        idEvents = intent.getStringExtra("idEvents");
        statusBtnMain = intent.getBooleanExtra("status",true);
        image = intent.getStringExtra("image");
        statusButton(statusBtnMain);

        mCircleImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        mAlertDialog = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Espere un momento . . .")
                .setCancelable(false)
                .build();
        mBuilderSelector = new AlertDialog.Builder(this);
        mBuilderSelector.setTitle("Selecciona una opción");
        options = new  CharSequence[] {"Imagen de galeria" , "Tomar Foto"};

        mImageViewEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectOptionImage(1);
            }
        });
        mButtonEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickEvents();
            }
        });

        mButtonUpdateEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateEvents();
            }
        });
    }

    private void statusButton(Boolean statusButtonMaintainer) {
        if(!statusButtonMaintainer){
            this.mButtonEvents.setEnabled(false);
            this.mButtonEvents.setVisibility(View.INVISIBLE);
            this.mButtonUpdateEvents.setEnabled(true);
            this.mButtonUpdateEvents.setVisibility(View.VISIBLE);
            this.mTextInputTitle.setText(getIntent().getStringExtra("title"));
            this.mTextInputDescription.setText(getIntent().getStringExtra("description"));
            this.mTextInputDirection.setText(getIntent().getStringExtra("direction"));
            this.mTextInputPrice.setText(getIntent().getStringExtra("price"));
            this.mTextInputIntervalTime.setText(getIntent().getStringExtra("intervalTime"));
            this.mTextInputCellphone.setText(getIntent().getStringExtra("cellphone"));
            this.mTextInputWhatsapp.setText(getIntent().getStringExtra("whatsapp"));
            Picasso.with(MantenedorEventsActivity.this).load(image).into(this.mImageViewEvents);
        }
    }

    private void clickEvents() {
        mTitle = mTextInputTitle.getText().toString();
        mDescription = mTextInputDescription.getText().toString();
        mDirection = mTextInputDirection.getText().toString();
        mPrice = mTextInputPrice.getText().toString();
        mIntervalTime = mTextInputIntervalTime.getText().toString();
        mCellphone = mTextInputCellphone.getText().toString();
        mWhatsapp = mTextInputWhatsapp.getText().toString();
        if(!mTitle.isEmpty() && !mDescription.isEmpty() && !mDirection.isEmpty() && !mPrice.isEmpty() && !mIntervalTime.isEmpty() && mCellphone.length()==9 && mWhatsapp.length()==9){
            if(mImageFile!=null){
                saveImage(mImageFile);
            }
            else if (mPhotoFile != null ) {
                saveImage(mPhotoFile);
            }
            else{
                Toast.makeText(this, "Debes seleccionar una imagen", Toast.LENGTH_SHORT).show();
            }
        }else if(mTitle.isEmpty()){
            if(mDescription.isEmpty() && mDirection.isEmpty() && mPrice.isEmpty() && mIntervalTime.isEmpty() && mCellphone.length()!=9 && mWhatsapp.length()!=9){
                mTextInputTitle.setError("Campo obligatorio");
                mTextInputDescription.setError("Campo obligatorio");
                mTextInputDirection.setError("Campo obligatorio");
                mTextInputPrice.setError("Campo obligatorio");
                mTextInputIntervalTime.setError("Campo obligatorio");
                mTextInputCellphone.setError("9 dígitos requerido");
                mTextInputWhatsapp.setError("9 dígitos requerido");
                mTextInputTitle.requestFocus();
            }else{
                mTextInputTitle.setError("Campo obligatorio");
                mTextInputTitle.requestFocus();
            }
        }else if(mDescription.isEmpty()){
            if(mDirection.isEmpty() && mPrice.isEmpty() && mIntervalTime.isEmpty() && mCellphone.length()!=9 && mWhatsapp.length()!=9){
                mTextInputDescription.setError("Campo obligatorio");
                mTextInputDirection.setError("Campo obligatorio");
                mTextInputPrice.setError("Campo obligatorio");
                mTextInputIntervalTime.setError("Campo obligatorio");
                mTextInputCellphone.setError("9 dígitos requerido");
                mTextInputWhatsapp.setError("9 dígitos requerido");
                mTextInputDescription.requestFocus();
            }else{
                mTextInputDescription.setError("Campo obligatorio");
                mTextInputDescription.requestFocus();
            }
        }else if(mDirection.isEmpty()){
            if(mPrice.isEmpty() && mIntervalTime.isEmpty() && mCellphone.length()!=9 && mWhatsapp.length()!=9){
                mTextInputDirection.setError("Campo obligatorio");
                mTextInputPrice.setError("Campo obligatorio");
                mTextInputIntervalTime.setError("Campo obligatorio");
                mTextInputCellphone.setError("9 dígitos requerido");
                mTextInputWhatsapp.setError("9 dígitos requerido");
                mTextInputDirection.requestFocus();
            }else{
                mTextInputDirection.setError("Campo obligatorio");
                mTextInputDirection.requestFocus();
            }
        }else if(mPrice.isEmpty()){
            if(mIntervalTime.isEmpty() && mCellphone.length()!=9 && mWhatsapp.length()!=9){
                mTextInputPrice.setError("Campo obligatorio");
                mTextInputIntervalTime.setError("Campo obligatorio");
                mTextInputCellphone.setError("9 dígitos requerido");
                mTextInputWhatsapp.setError("9 dígitos requerido");
                mTextInputPrice.requestFocus();
            }else{
                mTextInputPrice.setError("Campo obligatorio");
                mTextInputPrice.requestFocus();
            }
        }else if(mIntervalTime.isEmpty()){
            if(mCellphone.length()!=9 && mWhatsapp.length()!=9){
                mTextInputIntervalTime.setError("Campo obligatorio");
                mTextInputCellphone.setError("9 dígitos requerido");
                mTextInputWhatsapp.setError("9 dígitos requerido");
                mTextInputIntervalTime.requestFocus();
            }else{
                mTextInputIntervalTime.setError("Campo obligatorio");
                mTextInputIntervalTime.requestFocus();
            }
        }else if(mCellphone.length()!=9){
            if(mWhatsapp.length()!=9){
                mTextInputCellphone.setError("9 dígitos requerido");
                mTextInputWhatsapp.setError("9 dígitos requerido");
                mTextInputCellphone.requestFocus();
            }else{
                mTextInputCellphone.setError("9 dígitos requerido");
                mTextInputCellphone.requestFocus();
            }
        }else if(mWhatsapp.length()!=9){
            mTextInputWhatsapp.setError("9 dígitos requerido");
            mTextInputWhatsapp.requestFocus();
        }
    }//end clickEvents

    private void updateEvents() {
        mTitle = mTextInputTitle.getText().toString();
        mDescription = mTextInputDescription.getText().toString();
        mDirection = mTextInputDirection.getText().toString();
        mPrice = mTextInputPrice.getText().toString();
        mIntervalTime = mTextInputIntervalTime.getText().toString();
        mCellphone = mTextInputCellphone.getText().toString();
        mWhatsapp = mTextInputWhatsapp.getText().toString();
        if(!mTitle.isEmpty() && !mDescription.isEmpty() && !mDirection.isEmpty() && !mPrice.isEmpty() && !mIntervalTime.isEmpty() && mCellphone.length()==9 && mWhatsapp.length()==9){
            if(mImageFile!=null){
                updateImage2(mImageFile );
            }
            else if (mPhotoFile != null ) {
                updateImage2(mPhotoFile);
            }
            else{
                updateImage1();
            }
        }else if(mTitle.isEmpty()){
            if(mDescription.isEmpty() && mDirection.isEmpty() && mPrice.isEmpty() && mIntervalTime.isEmpty() && mCellphone.length()!=9 && mWhatsapp.length()!=9){
                mTextInputTitle.setError("Campo obligatorio");
                mTextInputDescription.setError("Campo obligatorio");
                mTextInputDirection.setError("Campo obligatorio");
                mTextInputPrice.setError("Campo obligatorio");
                mTextInputIntervalTime.setError("Campo obligatorio");
                mTextInputCellphone.setError("9 dígitos requerido");
                mTextInputWhatsapp.setError("9 dígitos requerido");
                mTextInputTitle.requestFocus();
            }else{
                mTextInputTitle.setError("Campo obligatorio");
                mTextInputTitle.requestFocus();
            }
        }else if(mDescription.isEmpty()){
            if(mDirection.isEmpty() && mPrice.isEmpty() && mIntervalTime.isEmpty() && mCellphone.length()!=9 && mWhatsapp.length()!=9){
                mTextInputDescription.setError("Campo obligatorio");
                mTextInputDirection.setError("Campo obligatorio");
                mTextInputPrice.setError("Campo obligatorio");
                mTextInputIntervalTime.setError("Campo obligatorio");
                mTextInputCellphone.setError("9 dígitos requerido");
                mTextInputWhatsapp.setError("9 dígitos requerido");
                mTextInputDescription.requestFocus();
            }else{
                mTextInputDescription.setError("Campo obligatorio");
                mTextInputDescription.requestFocus();
            }
        }else if(mDirection.isEmpty()){
            if(mPrice.isEmpty() && mIntervalTime.isEmpty() && mCellphone.length()!=9 && mWhatsapp.length()!=9){
                mTextInputDirection.setError("Campo obligatorio");
                mTextInputPrice.setError("Campo obligatorio");
                mTextInputIntervalTime.setError("Campo obligatorio");
                mTextInputCellphone.setError("9 dígitos requerido");
                mTextInputWhatsapp.setError("9 dígitos requerido");
                mTextInputDirection.requestFocus();
            }else{
                mTextInputDirection.setError("Campo obligatorio");
                mTextInputDirection.requestFocus();
            }
        }else if(mPrice.isEmpty()){
            if(mIntervalTime.isEmpty() && mCellphone.length()!=9 && mWhatsapp.length()!=9){
                mTextInputPrice.setError("Campo obligatorio");
                mTextInputIntervalTime.setError("Campo obligatorio");
                mTextInputCellphone.setError("9 dígitos requerido");
                mTextInputWhatsapp.setError("9 dígitos requerido");
                mTextInputPrice.requestFocus();
            }else{
                mTextInputPrice.setError("Campo obligatorio");
                mTextInputPrice.requestFocus();
            }
        }else if(mIntervalTime.isEmpty()){
            if(mCellphone.length()!=9 && mWhatsapp.length()!=9){
                mTextInputIntervalTime.setError("Campo obligatorio");
                mTextInputCellphone.setError("9 dígitos requerido");
                mTextInputWhatsapp.setError("9 dígitos requerido");
                mTextInputIntervalTime.requestFocus();
            }else{
                mTextInputIntervalTime.setError("Campo obligatorio");
                mTextInputIntervalTime.requestFocus();
            }
        }else if(mCellphone.length()!=9){
            if(mWhatsapp.length()!=9){
                mTextInputCellphone.setError("9 dígitos requerido");
                mTextInputWhatsapp.setError("9 dígitos requerido");
                mTextInputCellphone.requestFocus();
            }else{
                mTextInputCellphone.setError("9 dígitos requerido");
                mTextInputCellphone.requestFocus();
            }
        }else if(mWhatsapp.length()!=9){
            mTextInputWhatsapp.setError("9 dígitos requerido");
            mTextInputWhatsapp.requestFocus();
        }
    }//end updateEvents

    private void saveImage(File imageFile1) {
        mAlertDialog.show();
        mImageProvider.save(MantenedorEventsActivity.this,imageFile1).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()){
                    mImageProvider.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            final String url = uri.toString();
                            Events events = new Events();
                            events.setImage(url);
                            events.setId(UUID.randomUUID().toString());
                            events.setTitle(mTitle);
                            events.setDescription(mDescription);
                            events.setDirection(mDirection);
                            events.setPrice(mPrice);
                            events.setIntervalTime(mIntervalTime);
                            events.setCellphone(mCellphone);
                            events.setWhatsapp(mWhatsapp);
                            events.setIdUser(mAutProvider.getUid());
                            mEventsProvider.save(events).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> taskSave) {
                                                        mAlertDialog.dismiss();
                                                        if (taskSave.isSuccessful()){
                                                            //clearForm();
                                                            finish();
                                                            Toast.makeText(MantenedorEventsActivity.this, "Se almacenó correctamente", Toast.LENGTH_SHORT).show();
                                                        }else{
                                                            Toast.makeText(MantenedorEventsActivity.this, "No se pudo alamacenar la información", Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                            }//end onSuccess
                                        });
                }else{
                    mAlertDialog.dismiss();
                    Toast.makeText(MantenedorEventsActivity.this, "Error al almacenar la imagen", Toast.LENGTH_LONG).show();
                }
            }
        });

    }//end saveImage

    private void updateImage1() {
        mAlertDialog.show();
        Events events = new Events();
        events.setImage(image);
        events.setId(idEvents);
        events.setTitle(mTitle);
        events.setDescription(mDescription);
        events.setDirection(mDirection);
        events.setPrice(mPrice);
        events.setIntervalTime(mIntervalTime);
        events.setCellphone(mCellphone);
        events.setWhatsapp(mWhatsapp);
        events.setIdUser(mAutProvider.getUid());
        mEventsProvider.update(events).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> taskSave) {
                mAlertDialog.dismiss();
                if (taskSave.isSuccessful()){
                    finish();
                    Toast.makeText(MantenedorEventsActivity.this, "Se actualizó correctamente", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(MantenedorEventsActivity.this, "No se pudo actualizar la información", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }//end updateImage1

    private void updateImage2(File imageFile1) {
        mAlertDialog.show();
        mImageProvider.save(MantenedorEventsActivity.this,imageFile1).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()){
                    mImageProvider.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            final String url = uri.toString();
                            Events events = new Events();
                            events.setImage(url);
                            events.setId(idEvents);
                            events.setTitle(mTitle);
                            events.setDescription(mDescription);
                            events.setDirection(mDirection);
                            events.setPrice(mPrice);
                            events.setIntervalTime(mIntervalTime);
                            events.setCellphone(mCellphone);
                            events.setWhatsapp(mWhatsapp);
                            events.setIdUser(mAutProvider.getUid());
                            mEventsProvider.update(events).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> taskSave) {
                                    mAlertDialog.dismiss();
                                    if (taskSave.isSuccessful()){
                                        finish();
                                        Toast.makeText(MantenedorEventsActivity.this, "Se actualizó correctamente", Toast.LENGTH_SHORT).show();
                                    }else{
                                        Toast.makeText(MantenedorEventsActivity.this, "No se pudo actualizar la información", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }//end onSuccess
                    });
                }else{
                    mAlertDialog.dismiss();
                    Toast.makeText(MantenedorEventsActivity.this, "Error al actualizar la imagen", Toast.LENGTH_LONG).show();
                }
            }
        });

    }//end saveImage

    private void clearForm() {
        mTextInputTitle.setText("");
        mTextInputDescription.setText("");
        mTextInputDirection.setText("");
        mTextInputPrice.setText("");
        mTextInputIntervalTime.setText("");
        mTextInputCellphone.setText("");
        mTextInputWhatsapp.setText("");
        mImageViewEvents.setImageResource(R.drawable.ic_camera_24);
        mTitle =  "";
        mDescription = "";
        mDirection = "";
        mPrice = "";
        mIntervalTime = "";
        mCellphone = "";
        mWhatsapp = "";
        mImageFile = null;
    }

    private void selectOptionImage(final int numberImage) {
        mBuilderSelector.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == 0) {
                    if (numberImage == 1) {
                        openGalery(GALLERY_REQUEST_CODE);
                    }
                }
                else if (i == 1){
                    if (numberImage == 1) {
                        takePhoto(PHOTO_REQUEST_CODE);
                    }
                }
            }
        });
        mBuilderSelector.show();
    }

    private void openGalery(int requestCode) {
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent,requestCode);
    }

    private void takePhoto( int requestCode) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createPhotoFile(requestCode);
            } catch(Exception e) {
                Toast.makeText(this, "Hubo un error con el archivo " + e.getMessage(), Toast.LENGTH_LONG).show();
            }

            if (photoFile != null) {
                Uri photoUri = FileProvider.getUriForFile(MantenedorEventsActivity.this, "com.terrazas.terrazasmallap", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(takePictureIntent, requestCode);
            }
        } Toast.makeText(this, "Seleccionó tomo foto", Toast.LENGTH_SHORT).show();
    }

    private File createPhotoFile(int requestCode) throws IOException {
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File photoFile = File.createTempFile(
                new Date() + "_photo",
                ".jpg",
                storageDir
        );
        if (requestCode == PHOTO_REQUEST_CODE) {
            mPhotoPath = "file:" + photoFile.getAbsolutePath();
            mAbsolutePhotoPath = photoFile.getAbsolutePath();
        }

        return photoFile;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK){
            try {
                mPhotoFile = null;
                mImageFile = FileUtil.from(this,data.getData());
                mImageViewEvents.setImageBitmap(BitmapFactory.decodeFile(mImageFile.getAbsolutePath()));
            }catch (Exception e){
                Log.d("Error", "se produjo un error" + e.getMessage());
                Toast.makeText(this, "Se produjo un error" + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }

        /**
         * SELECCION DE FOTOGRAFIA
         */
        if (requestCode == PHOTO_REQUEST_CODE && resultCode == RESULT_OK) {
            mImageFile = null;
            mPhotoFile = new File(mAbsolutePhotoPath);
            Picasso.with(MantenedorEventsActivity.this).load(mPhotoPath).into(mImageViewEvents);
        }
    }
}
