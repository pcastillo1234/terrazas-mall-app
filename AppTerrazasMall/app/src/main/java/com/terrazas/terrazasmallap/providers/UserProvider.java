package com.terrazas.terrazasmallap.providers;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.terrazas.terrazasmallap.models.User;

import java.util.HashMap;
import java.util.Map;

public class UserProvider {

    private FirebaseFirestore mFirestore;
    private CollectionReference mColecction;

    public UserProvider( ) {
        mColecction = FirebaseFirestore.getInstance().collection("Users");
    }

    public Task<DocumentSnapshot> getUser(String id){
        return mColecction.document(id).get();
    }

    public  Task<Void> create(User user){
        return  mColecction.document(user.getId()).set(user);
    }

    public  Task<Void> update(User user){
        Map<String, Object> map = new HashMap<>();
        map.put("username",user.getUsername());
        return  mColecction.document(user.getId()).update(map);
    }

    public Task<Void> updateUserProfile(User user){
        return mColecction.document(user.getId())
                .update("username",user.getUsername(),
                        "dni",user.getDni(),
                        "cellphone",user.getCellphone());
    }

}
