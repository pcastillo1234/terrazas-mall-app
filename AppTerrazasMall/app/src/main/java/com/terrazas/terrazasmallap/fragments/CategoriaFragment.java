package com.terrazas.terrazasmallap.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.Query;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.activities.MantenedorCategoryActivity;
import com.terrazas.terrazasmallap.adapters.CategoryAdapter;
import com.terrazas.terrazasmallap.models.Category;
import com.terrazas.terrazasmallap.providers.AuthProvider;
import com.terrazas.terrazasmallap.providers.CategoryProvider;

public class CategoriaFragment extends Fragment {

    View mView;
    FloatingActionButton mFab;
    RecyclerView mRecyclerView;
    CategoryProvider mCategoryProvider;
    AuthProvider mAuthProvider;
    CategoryAdapter mCategoryAdapter;

    public CategoriaFragment() { } //needed for firebase

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_categoria, container, false);
        mFab = mView.findViewById(R.id.fabCategory);
        mRecyclerView = mView.findViewById(R.id.riclerViewCategory);
        mCategoryProvider = new CategoryProvider();
        mAuthProvider = new AuthProvider();

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(),2);
        mRecyclerView.setLayoutManager(gridLayoutManager);

        validarCorreo();

        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToCategory();
            }
        });

        return mView;
    }

    public void validarCorreo(){
        if(!mAuthProvider.getEmail().equals("terrazashco@hotmail.com")){
            mFab.setEnabled(false);
            mFab.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Query query = mCategoryProvider.getAll();
        FirestoreRecyclerOptions<Category> options =
                new FirestoreRecyclerOptions.Builder<Category>()
                        .setQuery(query,Category.class)
                        .build();

        mCategoryAdapter = new CategoryAdapter(options,getContext());
        mRecyclerView.setAdapter(mCategoryAdapter);
        mCategoryAdapter.startListening();
        Log.d("email",mAuthProvider.getEmail());
        //back press
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.container, new CategoriaFragment());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this,callback);
    }
    @Override
    public void onStop() {
        super.onStop();
        mCategoryAdapter.stopListening();
    }

    private void goToCategory() {
        Intent intent = new Intent(getContext(),MantenedorCategoryActivity.class);
        intent.putExtra("status",true);
        startActivity(intent);
    }
}
