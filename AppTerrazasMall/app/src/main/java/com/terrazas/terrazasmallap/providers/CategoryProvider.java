package com.terrazas.terrazasmallap.providers;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.terrazas.terrazasmallap.models.Category;

public class CategoryProvider {
    CollectionReference mCollection;

    public CategoryProvider(){
        mCollection = FirebaseFirestore.getInstance().collection("Category");
    }

    public Task<Void> save(Category category){
        return mCollection.document(category.getId()).set(category);
    }

    public Task<Void> update(Category category){
        return mCollection.document(category.getId())
                .update("title",category.getTitle(),
                        "image",category.getImage(),
                        "idUser",category.getIdUser());
    }

    public Task<Void> delete(String idCategory){
        return  mCollection.document(idCategory).delete();
    }

    public Query getAll(){
        return mCollection.orderBy("title", Query.Direction.DESCENDING);
    }
}
