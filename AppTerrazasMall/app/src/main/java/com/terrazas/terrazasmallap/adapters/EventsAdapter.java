package com.terrazas.terrazasmallap.adapters;



import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.squareup.picasso.Picasso;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.activities.MantenedorCategoryActivity;
import com.terrazas.terrazasmallap.activities.MantenedorEventsActivity;
import com.terrazas.terrazasmallap.fragments.EventDetailFragment;
import com.terrazas.terrazasmallap.models.Events;
import com.terrazas.terrazasmallap.providers.AuthProvider;
import com.terrazas.terrazasmallap.providers.EventsProvider;

public class EventsAdapter extends FirestoreRecyclerAdapter<Events, EventsAdapter.ViewHolder> {
   Context context;
   EventsProvider eventsProvider;
   AuthProvider mAuthProvider;
   AlertDialog.Builder alertDialog;
    public EventsAdapter(FirestoreRecyclerOptions<Events> options, Context context){
        super(options);
        this.context = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Events events) {
        holder.textViewtitle.setText(events.getTitle());
        holder.textViewDescription.setText(events.getDescription());
        holder.textViewprice.setText("S/"+events.getPrice());
        if(events.getImage() != null){
            if(!events.getImage().isEmpty()){
                Picasso.with(context).load(events.getImage()).into(holder.imageViewEvents);
            }
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppCompatActivity activity =(AppCompatActivity)view.getContext();
                activity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container,new EventDetailFragment(events.getImage(),events.getPrice(),events.getIntervalTime(),events.getDirection(),events.getDescription(),events.getCellphone(),events.getWhatsapp()))
                        .addToBackStack(null)
                        .commit();
            }
        });

        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), MantenedorEventsActivity.class);
                intent.putExtra("status",false);
                intent.putExtra("idEvents",events.getId());
                intent.putExtra("title",events.getTitle());
                intent.putExtra("image",events.getImage());
                intent.putExtra("description",events.getDescription());
                intent.putExtra("direction",events.getDirection());
                intent.putExtra("cellphone",events.getCellphone());
                intent.putExtra("whatsapp",events.getWhatsapp());
                intent.putExtra("price",events.getPrice());
                intent.putExtra("intervalTime",events.getIntervalTime());
                view.getContext().startActivity(intent);
            }//end onClick
        });

        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog = new AlertDialog.Builder(view.getContext());
                alertDialog.setMessage("¿Realmente desea eliminar "+events.getTitle()+"?")
                        .setCancelable(true)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                eventsProvider.delete(events.getId())
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if(task.isSuccessful()){
                                                    Toast.makeText(view.getContext(),"Sala de Eventos eliminada correctamente",Toast.LENGTH_SHORT).show();
                                                }else{
                                                    Toast.makeText(view.getContext(),"Error al eliminar Sala de Eventos",Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        })
                        .create()
                        .show();
            }//end onClick
        });

        holder.ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String message = "";
                message +=events.getTitle();
                message +="\n"+"Descripción: "+events.getDescription();
                message +="\n"+"Ubícanos en "+events.getDirection();
                message +="\n"+"Comunícate con nosotros llamando al "+events.getCellphone();
                message +="\n"+"Descarga nuestra aplicación y conoce más sobre lo que brindamos:";
                message +="\n"+"link";
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT, message);
                view.getContext().startActivity(Intent.createChooser(share, "Compartir sala de eventos mediante:"));
            }//end onClick
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_events,parent,false);
        return new EventsAdapter.ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView textViewtitle;
        TextView textViewprice;
        TextView textViewDescription;
        ImageView imageViewEvents;
        CardView cardView;
        ImageView ivEdit;
        ImageView ivDelete;
        ImageView ivShare;

        public ViewHolder(View view){
            super(view);
            textViewtitle = view.findViewById(R.id.txtTitleCVSE);
            textViewprice = view.findViewById(R.id.txtPriceCVSE);
            textViewDescription = view.findViewById(R.id.txtDescriptionCVSE);
            imageViewEvents= view.findViewById(R.id.ivImageCVSE);
            cardView = view.findViewById(R.id.cardViewE);
            ivEdit = view.findViewById(R.id.ibEditEvent);
            ivDelete = view.findViewById(R.id.ibDeleteEvent);
            ivShare = view.findViewById(R.id.ibShareEvent);

            eventsProvider = new EventsProvider();
            mAuthProvider = new AuthProvider();

            validarCorreo();
        }
        public void validarCorreo(){
            if(!mAuthProvider.getEmail().equals("terrazashco@hotmail.com")){
                //update
                ivEdit.setEnabled(false);
                ivEdit.setVisibility(View.INVISIBLE);
                //delete
                ivDelete.setEnabled(false);
                ivDelete.setVisibility(View.INVISIBLE);
            }
        }
    }//end ViewHolder
}
