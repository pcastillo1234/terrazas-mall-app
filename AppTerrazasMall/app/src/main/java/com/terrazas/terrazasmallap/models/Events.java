package com.terrazas.terrazasmallap.models;

public class Events {
    private String id;
    private String title;
    private String description;
    private String direction;
    private String price;
    private String intervalTime;
    private String cellphone;
    private String whatsapp;
    private String image;
    private String idUser;

    public Events() {
    }

    public Events(String id, String title, String description, String direction, String price, String intervalTime, String cellphone, String whatsapp, String image, String idUser) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.direction = direction;
        this.price = price;
        this.intervalTime = intervalTime;
        this.cellphone = cellphone;
        this.whatsapp = whatsapp;
        this.image = image;
        this.idUser = idUser;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getIntervalTime() {
        return intervalTime;
    }

    public void setIntervalTime(String intervalTime) {
        this.intervalTime = intervalTime;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getWhatsapp() {
        return whatsapp;
    }

    public void setWhatsapp(String whatsapp) {
        this.whatsapp = whatsapp;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }
}
