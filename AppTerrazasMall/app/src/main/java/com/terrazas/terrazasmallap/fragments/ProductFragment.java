package com.terrazas.terrazasmallap.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.Query;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.activities.MaintainerProductActivity;
import com.terrazas.terrazasmallap.adapters.ProductAdapter;
import com.terrazas.terrazasmallap.models.Product;
import com.terrazas.terrazasmallap.providers.AuthProvider;
import com.terrazas.terrazasmallap.providers.ProductProvider;

import java.security.Provider;

public class ProductFragment extends Fragment {
    View mView;
    FloatingActionButton mFab;
    RecyclerView mRecyclerView;
    TextView txtTitleStand;
    ProductProvider mProductProvider;
    AuthProvider mAuthProvider;
    ProductAdapter mProductAdapter;
    String idStand;
    String titleStand;

    public ProductFragment() {
    }

    public ProductFragment(String idStand) {
        this.idStand=idStand;
    }

    public ProductFragment(String idStand, String titleStand) {
        this.idStand=idStand;
        this.titleStand=titleStand;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_product,container,false);
        mFab = mView.findViewById(R.id.fabP);
        mRecyclerView = mView.findViewById(R.id.recyclerViewProduct);
        txtTitleStand = mView.findViewById(R.id.lblProductStand);
        mProductProvider = new ProductProvider();
        mAuthProvider = new AuthProvider();

        standTitle();

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(),3);
        mRecyclerView.setLayoutManager(gridLayoutManager);

        validarCorreo();

        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoProduct();
            }
        });

        return mView;
    }

    private void standTitle() {
        if(!titleStand.isEmpty()){
            txtTitleStand.setText(ucFirst(titleStand));
        }
    }

    public static String ucFirst(String str) {
        if (str == null || str.isEmpty()) return str;
        else return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    public void validarCorreo(){
        if(!mAuthProvider.getEmail().equals("terrazashco@hotmail.com")){
            mFab.setEnabled(false);
            mFab.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Query query = mProductProvider.getAll(idStand);
        FirestoreRecyclerOptions<Product> options =
                new FirestoreRecyclerOptions.Builder<Product>()
                        .setQuery(query, Product.class)
                        .build();

        mProductAdapter = new ProductAdapter(options,getContext());
        mRecyclerView.setAdapter(mProductAdapter);
        mProductAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        mProductAdapter.stopListening();
    }

    private void gotoProduct() {
        Intent intent = new Intent(getContext(), MaintainerProductActivity.class);
        intent.putExtra("idStand",idStand);
        intent.putExtra("status",true);
        startActivity(intent);
    }
}
