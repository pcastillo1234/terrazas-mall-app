package com.terrazas.terrazasmallap.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.activities.CompletarPerfillActivity;
import com.terrazas.terrazasmallap.activities.HomeActivity;
import com.terrazas.terrazasmallap.activities.MainActivity;
import com.terrazas.terrazasmallap.activities.MantenedorEventsActivity;
import com.terrazas.terrazasmallap.providers.AuthProvider;

import dmax.dialog.SpotsDialog;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PerfilFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PerfilFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    View mView;
    TextView txtProfile;
    TextView txtMyReservation;
    TextView txtReservationList;
    TextView txtChangePassword;
    TextView txtTermsAndConditions;
    TextView txtSuggestionsBook;
    TextView txtLogout;
    TextView txtListSuggestionBook;
    AuthProvider mAuthProvider;
    AlertDialog mDialog;

    public PerfilFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PerfilFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PerfilFragment newInstance(String param1, String param2) {
        PerfilFragment fragment = new PerfilFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_perfil, container, false);
        txtProfile = mView.findViewById(R.id.lblProfile);
        txtMyReservation = mView.findViewById(R.id.lblMyReservation);
        txtReservationList = mView.findViewById(R.id.lblReservationList);
        txtChangePassword = mView.findViewById(R.id.lblChangePassword);
        txtTermsAndConditions = mView.findViewById(R.id.lblTAC);
        txtSuggestionsBook = mView.findViewById(R.id.lblSuggestionBook);
        txtLogout = mView.findViewById(R.id.lblLogout);
        txtListSuggestionBook = mView.findViewById(R.id.lblListSuggestionBook);
        mAuthProvider = new AuthProvider();

        mDialog = new SpotsDialog.Builder()
                .setContext(getContext())
                .setMessage("Cerrando Sesión . . .")
                .setCancelable(false)
                .build();

        txtLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Logout();
            }
        });

        txtSuggestionsBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogSB();
            }
        });

        txtProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogUP();
            }
        });

        txtChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogCP();
            }
        });

        txtTermsAndConditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogTAC();
            }
        });

        txtListSuggestionBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFragmentListSuggestionBook();
            }
        });

        txtMyReservation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFragmentListMyReservation();
            }
        });

        txtReservationList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFragmentReservationList();
            }
        });

        return mView;
    }


    private void Logout() {
        mDialog.show();
        getActivity().finish();
        mDialog.dismiss();
    }//end logout

    public void showDialogSB(){
        DialogSuggestionBook dialogSuggestionBook = new DialogSuggestionBook();
        dialogSuggestionBook.setCancelable(true);
        dialogSuggestionBook.show(getFragmentManager(),"tag");
    }

    public void showDialogUP(){
        DialogUserProfile dialogUserProfile = new DialogUserProfile();
        dialogUserProfile.setCancelable(true);
        dialogUserProfile.show(getFragmentManager(),"tag");
    }

    public void showDialogCP(){
        DialogChangePassword dialogChangePassword = new DialogChangePassword();
        dialogChangePassword.setCancelable(true);
        dialogChangePassword.show(getFragmentManager(),"tag");
    }

    public void showDialogTAC(){
        DialogTermsAndConditions dialogTermsAndConditions = new DialogTermsAndConditions();
        dialogTermsAndConditions.setCancelable(true);
        dialogTermsAndConditions.show(getFragmentManager(),"tag");
    }

    private void showFragmentListSuggestionBook() {
        AppCompatActivity appCompatActivity = (AppCompatActivity)this.getContext();
        appCompatActivity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.container,new ListSuggestionBookFragment())
                .addToBackStack(null)
                .commit();
    }

    private void showFragmentListMyReservation() {
        AppCompatActivity appCompatActivity = (AppCompatActivity)this.getContext();
        appCompatActivity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.container,new ListReservationForUserFragment())
                .addToBackStack(null)
                .commit();
    }

    private void showFragmentReservationList() {
        AppCompatActivity appCompatActivity = (AppCompatActivity)this.getContext();
        appCompatActivity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.container,new ListReservationFragment())
                .addToBackStack(null)
                .commit();
    }

    public void validarCorreo(){
        if(!mAuthProvider.getEmail().equals("terrazashco@hotmail.com")){
            //
            txtListSuggestionBook.setEnabled(false);
            txtListSuggestionBook.setVisibility(View.INVISIBLE);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, -115, 0, 0);
            txtListSuggestionBook.setLayoutParams(layoutParams);
            //
            txtReservationList.setEnabled(false);
            txtReservationList.setVisibility(View.INVISIBLE);
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams2.setMargins(0, -115, 0, 0);
            txtReservationList.setLayoutParams(layoutParams);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        validarCorreo();
        //back press
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.container, new PerfilFragment());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this,callback);
    }

}