package com.terrazas.terrazasmallap.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;
import com.terrazas.terrazasmallap.R;

import de.hdodenhof.circleimageview.CircleImageView;


public class EventDetailFragment extends Fragment {

    ImageView imageView;
    TextView txtPrice;
    TextView txtDirection;
    TextView txtDescription;
    CircleImageView circleImageView;
    String image;
    String price;
    String intervalTime;
    String direction;
    String description;
    String cellphone;
    String whatsapp;
    Button btnCellphone;
    Button btnWhatsapp;

    public EventDetailFragment() {
    }

    public EventDetailFragment(String image, String price, String intervalTime, String Direction, String Description,String Cellphone, String Whatsapp) {
        this.image = image;
        this.price = price;
        this.intervalTime = intervalTime;
        this.direction = Direction;
        this.description = Description;
        this.cellphone = Cellphone;
        this.whatsapp = Whatsapp;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_event_detail, container, false);
        imageView = root.findViewById(R.id.ivEDF);
        txtPrice = root.findViewById(R.id.lblPriceEDF);
        txtDirection = root.findViewById(R.id.lblDirectionEDF);
        txtDescription = root.findViewById(R.id.lblDescriptionEDF);
        circleImageView = root.findViewById(R.id.circleImageBackEDF);
        btnCellphone = root.findViewById(R.id.btnCellphoneEDF);
        btnWhatsapp = root.findViewById(R.id.btnWhatsAppEDF);

        txtPrice.setText("S/"+price+"("+intervalTime+" minutos)");
        txtDirection.setText(direction);
        txtDescription.setText(description);
        if(image != null){
            if(!image.isEmpty()){
                Picasso.with(getContext()).load(image).into(imageView);
            }
        }

        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPress();
            }
        });

        btnCellphone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToDial();
            }
        });

        btnWhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToWhatsapp();
            }
        });

        return root;
    }//end onCreateView

    private void goToWhatsapp() {
        String url = "https://api.whatsapp.com/send?phone="+"51"+cellphone;
        Intent go = new Intent(Intent.ACTION_VIEW);
        go.setData(Uri.parse(url));
        startActivity(go);
    }

    private void goToDial() {
        Intent i = new Intent(Intent.ACTION_DIAL);
        i.setData(Uri.parse((String.valueOf("tel:+51"+cellphone))));
        startActivity(i);
    }

    public void onBackPress(){
        AppCompatActivity activity = (AppCompatActivity)getContext();
        activity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.container,new EventsFragment())
                .addToBackStack(null)
                .commit();
    }//end onBackPress
}
