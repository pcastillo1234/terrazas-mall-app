package com.terrazas.terrazasmallap.providers;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.terrazas.terrazasmallap.models.Tac;

public class TacProvider {
    CollectionReference mCollection;

    public TacProvider(){
        mCollection = FirebaseFirestore.getInstance().collection("TermsAndConditions");
    }

    public Task<Void> save(Tac tac){
        return mCollection.document(tac.getId()).set(tac);
    }

    public Task<Void> update(Tac tac){
        return mCollection.document(tac.getId())
                .update("description",tac.getDescription());
    }

    public Task<QuerySnapshot> getData(){
        return mCollection.get();
    }
}
