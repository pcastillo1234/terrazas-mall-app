package com.terrazas.terrazasmallap.fragments;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.adapters.SuggestionBookAdapter;
import com.terrazas.terrazasmallap.models.ExcelExportReservation;
import com.terrazas.terrazasmallap.models.ExcelExportSuggestionBook;
import com.terrazas.terrazasmallap.models.SuggestionBook;
import com.terrazas.terrazasmallap.providers.SuggestionBookProvider;

import java.util.ArrayList;
import java.util.List;

import dmax.dialog.SpotsDialog;

public class ListSuggestionBookFragment extends Fragment {
    ImageButton ivBack;
    ImageButton ivExportDataExcel;
    ListView mSuggestionBookList;
    SuggestionBookProvider mSuggestionBookProvider;
    SuggestionBookAdapter mSuggestionBookAdapter;
    List<SuggestionBook> mList;
    AlertDialog.Builder alertDialog;
    AlertDialog mAlertDialog;

   /*String mDate;
    String mId;
    String mIssue;
    String mMessage;
    String mIdUser;*/

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_suggestion_book,container,false);
        ivBack = view.findViewById(R.id.ivBack);
        ivExportDataExcel = view.findViewById(R.id.ivExportData);
        mSuggestionBookList = view.findViewById(R.id.suggestionBookList);
        mSuggestionBookProvider = new SuggestionBookProvider();

        waitingDialog();

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                returnProfile();
            }
        });

        ivExportDataExcel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exportExcelSB();
            }//end onClick
        });

        return view;
    }

    private void exportExcelSB() {
        alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setMessage("Exportar a Excel las Sugerencias")
                .setCancelable(true)
                .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                                && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                            mAlertDialog.show();
                            ExcelExportSuggestionBook.export(mList);
                            mAlertDialog.dismiss();
                            Toast.makeText(getContext(),"Excel generado correctamente en el almacenamiento interno de su dispositivo.",Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getContext(),"Habilite los permisos para generar el documento",Toast.LENGTH_SHORT).show();
                            askForPermission(Manifest.permission.READ_EXTERNAL_STORAGE, 0);
                            askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, 1);
                        }
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        })
                .create()
                .show();
    }//end exportExcel

    private void waitingDialog() {
        mAlertDialog = new SpotsDialog.Builder()
                .setContext(getContext())
                .setMessage("Espere un momento . . .")
                .setCancelable(false)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();
        mSuggestionBookProvider.getAll().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
               mList = new ArrayList<>();
                if(task.isSuccessful()){
                    for(QueryDocumentSnapshot document : task.getResult()){
                        SuggestionBook mData = document.toObject(SuggestionBook.class);
                        mList.add(mData);
                    }//end for
                    mSuggestionBookAdapter = new SuggestionBookAdapter(getContext(),mList);
                    mSuggestionBookList.setAdapter(mSuggestionBookAdapter);
                }else{
                    Log.d("Error","Error getting documents: ",task.getException());
                }
            }
        });
        //back press
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.container, new PerfilFragment());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this,callback);
    }

/*private void loadData() {
        mSuggestionBookProvider.getAll().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    List<SuggestionBook> list = new ArrayList<SuggestionBook>();
                    for(QueryDocumentSnapshot query : task.getResult()){
                        mId = query.getId();
                        mIssue = query.getData().get("issue").toString();
                        mMessage = query.getData().get("message").toString();
                        mIdUser = query.getData().get("idUser").toString();
                        mDate = query.getData().get("date").toString();
                        list.add(new SuggestionBook(mId,mIssue,mMessage,mDate,mIdUser));
                        Log.d("Collection Data",mId+"|"+mIssue+"|"+mMessage+"|"+mDate+"|"+mIdUser);
                    }
                    for(SuggestionBook suggestionBook : list){
                        Log.d("SuggestionBook Data", suggestionBook.getId()+suggestionBook.getIssue());
                    }
                }
            }
        });
    }//end loadData*/

    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(getActivity(), permission) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, requestCode);
        } else {
            Log.d("Permission", permission + " habilitado.");
        }
    }

    private void returnProfile() {
        AppCompatActivity appCompatActivity = (AppCompatActivity)this.getContext();
        appCompatActivity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.container,new PerfilFragment())
                .addToBackStack(null)
                .commit();
    }
}
