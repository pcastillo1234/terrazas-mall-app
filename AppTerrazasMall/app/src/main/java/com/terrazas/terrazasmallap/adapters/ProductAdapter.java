package com.terrazas.terrazasmallap.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.squareup.picasso.Picasso;
import com.terrazas.terrazasmallap.R;
import com.terrazas.terrazasmallap.activities.MaintainerProductActivity;
import com.terrazas.terrazasmallap.fragments.ProductFragment;
import com.terrazas.terrazasmallap.models.Product;
import com.terrazas.terrazasmallap.providers.AuthProvider;
import com.terrazas.terrazasmallap.providers.ProductProvider;

public class ProductAdapter extends FirestoreRecyclerAdapter<Product, ProductAdapter.ViewHolder> {
    Context context;
    View mView;
    ProductProvider productProvider;
    AuthProvider mAuthProvider;
    AlertDialog.Builder alertDialog;
    public ProductAdapter(@NonNull FirestoreRecyclerOptions<Product> options, Context context) {
        super(options);
        this.context = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Product product) {
        holder.textViewtitle.setText(product.getTitle());
        holder.textViewprice.setText("S/"+product.getPrice());

        if(product.getImage() != null){
            if(!product.getImage().isEmpty()){
                Picasso.with(context).load(product.getImage()).into(holder.imageViewProduct);
            }
        }

        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), MaintainerProductActivity.class);
                intent.putExtra("status",false);
                intent.putExtra("idProduct",product.getId());
                intent.putExtra("title",product.getTitle());
                intent.putExtra("price",product.getPrice());
                intent.putExtra("image",product.getImage());
                view.getContext().startActivity(intent);
            }
        });

        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog = new AlertDialog.Builder(view.getContext());
                alertDialog.setMessage("¿Realmente desea eliminar el Producto "+product.getTitle()+"?")
                        .setCancelable(true)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                productProvider.delete(product.getId())
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if(task.isSuccessful()){
                                                    Toast.makeText(view.getContext(),"Producto eliminado correctamente",Toast.LENGTH_SHORT).show();
                                                }else{
                                                    Toast.makeText(view.getContext(),"Error al eliminar Producto",Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                        .create()
                        .show();
            }//end onClick
        });

    }// end onBindViewHolder

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_product,parent,false);
        return new  ProductAdapter.ViewHolder(mView);
    }//end onCreateViewHolder

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView textViewtitle;
        TextView textViewprice;
        ImageView imageViewProduct;
        ImageView ivEdit;
        ImageView ivDelete;
        public ViewHolder(View view){
            super(view);
            textViewtitle = view.findViewById(R.id.txtTitleCVProduct);
            textViewprice = view.findViewById(R.id.txtPriceCVProduct);
            imageViewProduct= view.findViewById(R.id.ivImageCVProduct);
            ivEdit = view.findViewById(R.id.ibEditProduct);
            ivDelete = view.findViewById(R.id.ibDeleteProduct);

            productProvider = new ProductProvider();
            mAuthProvider = new AuthProvider();

            validarCorreo();
        }
        public void validarCorreo(){
            if(!mAuthProvider.getEmail().equals("terrazashco@hotmail.com")){
                //update
                ivEdit.setEnabled(false);
                ivEdit.setVisibility(View.INVISIBLE);
                //delete
                ivDelete.setEnabled(false);
                ivDelete.setVisibility(View.INVISIBLE);
            }
        }
    }//end viewHolder
}
