package com.terrazas.terrazasmallap.models;

public class Schedule {
    private String idSchedule;
    private String time;

    public Schedule() {
    }

    public Schedule(String idSchedule, String time) {
        this.idSchedule = idSchedule;
        this.time = time;
    }

    public String getIdSchedule() {
        return idSchedule;
    }

    public void setIdSchedule(String idSchedule) {
        this.idSchedule = idSchedule;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
